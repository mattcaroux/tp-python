"""Init Dev : TP10"""

# =====================================================================
# Exercice 1 : Choix de modélisation et complexité
# =====================================================================
# Modélisation n°1
# =====================================================================

# Penser à completer la fonction exemples_pokedex_v1 dans le fichier de tests

def appartient_v1(pokemon, pokedex): 
    """ renvoie True si pokemon (str) est présent dans le pokedex """
    for couple_poke in pokedex:
        if couple_poke[0] == pokemon:
            return True
    return False


def toutes_les_attaques_v1(pokemon, pokedex): 
    """
    param: un pokedex et le nom d'un pokemon (str) qui appartient au pokedex
    resultat: renvoie l'ensemble des types d'attaque du pokemon passé en paramètre
    """
    res = set()
    for couple_pokemon in pokedex:
        if couple_pokemon[0] == pokemon:
            for i in range(1, len(couple_pokemon)):
                res.add(couple_pokemon[i])
    return res


def nombre_de_v1(attaque, pokedex): 
    """
    param: un pokedex et un type d'attaque (str)
    resultat: renvoie le nombre de pokemons de ce type d'attaque
    dans le pokedex
    """
    res = 0
    for (_, atq) in pokedex:
        if attaque == atq:
            res += 1
    return res



def attaque_preferee_v1(pokedex):
    """
    Renvoie le nom du type d'attaque qui est la plus fréquente dans le pokedex
    """
    res = dict()
    max = 0
    atq_max = None
    for (pokemon, attaque) in pokedex:
        if attaque in res.keys():
            res[attaque] += 1
        else:
            res[attaque] = 1
        if res[attaque] > max:
            atq_max = attaque
            max = res[attaque]
    return atq_max


# =====================================================================
# Modélisation n°2
# =====================================================================

# Penser à completer la fonction exemples_pokedex_v2 dans le fichier de tests

def appartient_v2(pokemon, pokedex):
    """ renvoie True si pokemon (str) est présent dans le pokedex """
    return pokemon in pokedex.keys()


def toutes_les_attaques_v2(pokemon, pokedex):
    """
    param: un pokedex et le nom d'un pokemon (str) qui appartient au pokedex
    resultat: renvoie l'ensemble des types d'attaque du pokemon passé en paramètre
    """
    return pokedex[pokemon]


def nombre_de_v2(attaque, pokedex):
    """
    param: un pokedex et un type d'attaque (str)
    resultat: renvoie le nombre de pokemons de ce type d'attaque
    dans le pokedex
    """
    res = 0
    for (pokemon, type) in pokedex.items():
        if attaque in type:
            res += 1
    return res


def attaque_preferee_v2(pokedex):
    """
    Renvoie le nom du type d'attaque qui est la plus fréquente dans le pokedex
    """
    max = 0
    atq_max = None
    dico_freq = dict()
    for attaques in pokedex.values():
        for attaque in attaques:
            if attaque not in dico_freq.keys():
                dico_freq[attaque] = 1
            else:
                dico_freq[attaque] += 1
            if dico_freq[attaque] > max:
                max = dico_freq[attaque]
                atq_max = attaque
    return atq_max


# =====================================================================
# Modélisation n°3
# =====================================================================

# Penser à completer la fonction exemples_pokedex_v3 dans le fichier de tests


def appartient_v3(pokemon, pokedex):
    """ renvoie True si pokemon (str) est présent dans le pokedex """
    for pokemons in pokedex.values():
        if pokemon in pokemons:
            return True
    return False


def toutes_les_attaques_v3(pokemon, pokedex):
    """
    param: un pokedex et le nom d'un pokemon (str) qui appartient au pokedex
    resultat: renvoie l'ensemble des types d'attaque du pokemon passé en paramètre
    """
    res = set()
    for (attaque, pokemons) in pokedex.items():
        if pokemon in pokemons:
            res.add(attaque)
    return res


def nombre_de_v3(attaque, pokedex):
    """
    param: un pokedex et un type d'attaque (str)
    resultat: renvoie le nombre de pokemons de ce type d'attaque
    dans le pokedex
    """
    ensemble_pokemon = set()
    for (atq, pokemons) in pokedex.items():
        if atq == attaque:
            ensemble_pokemon = ensemble_pokemon | pokemons
    return len(ensemble_pokemon)


def attaque_preferee_v3(pokedex):
    """
    Renvoie le nom du type d'attaque qui est la plus fréquente dans le pokedex
    """
    max = 0
    atq_max = None
    for attaque, pokemons in pokedex.items():
        if len(pokemons) > max:
            max = len(pokemons)
            atq_max = attaque
    return atq_max


# =====================================================================
# Transformations
# =====================================================================

# Version 1 ==> Version 2

def v1_to_v2(pokedex_v1):
    """
    param: prend en paramètre un pokedex version 1
    renvoie le même pokedex mais en version 2
    """
    pokedex_v2 = dict()
    for (pokemon, type) in pokedex_v1:
        if pokemon not in pokedex_v2.keys():
            pokedex_v2[pokemon] = set()
        pokedex_v2[pokemon].add(type)
    return pokedex_v2

# Version 2 ==> Version 3

def v2_to_v3(pokedex_v2):
    """
    param: prend en paramètre un pokedex version2
    renvoie le même pokedex mais en version3
    """
    pokedex_v3 = dict()
    for (pokemon, types) in pokedex_v2.items():
        for type in types:
            if type not in pokedex_v3:
                pokedex_v3[type] = set()
            pokedex_v3[type].add(pokemon)
    return pokedex_v3