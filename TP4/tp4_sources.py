def plus_long_plateau(chaine):
    """recherche la longueur du plus grand plateau d'une chaine
    Args:
        chaine (str): une chaine de caractères

    Returns:
        int: la longueur de la plus grande suite de lettres consécutives égales
    """
    lg_max = 0  # longueur du plus grand plateau déjà trouvé
    lg_actuelle = 0  # longueur du plateau actuel
    prec = ''  # caractère précédent dans la chaine
    for lettre in chaine:
        if lettre == prec:  # si la lettre actuelle est égale à la précédente
            lg_actuelle += 1
        else:  # si la lettre actuelle est différente de la précédente
            if lg_actuelle > lg_max:
                lg_max = lg_actuelle
            lg_actuelle = 1
        prec = lettre
    if lg_actuelle > lg_max:  # cas du dernier plateau
        lg_max = lg_actuelle
    return lg_max





def plus_long_plateau_matthias(chaine):
    """recherche la longueur du plus grand plateau d'une chaine
    Args:
        chaine (str): une chaine de caractères

    Returns:
        int: la longueur de la plus grande suite de lettres consécutives égales
    """
    lg_max = 0
    lg_actuelle = 0
    for i in range(len(chaine)):
        if chaine[i] == chaine[i-1]:
            lg_actuelle += 1
        else:
            if lg_actuelle >lg_max:
                lg_max = lg_actuelle
            lg_actuelle = 1#on remet lg actuelle a 1 car on reprend a la premiere lettre
    if lg_actuelle > lg_max:  # cas du dernier plateau
        lg_max = lg_actuelle
    return lg_max


def test_plus_long_plateau():
    assert plus_long_plateau_matthias("aaaabbbbbbbbb") == 9
    assert plus_long_plateau_matthias("aa") == 2
    assert plus_long_plateau_matthias("") == 0
    assert plus_long_plateau_matthias("  a") == 2

# --------------------------------------
# Exemple de villes avec leur population
# --------------------------------------
liste_villes = ["Blois", "Bourges", "Chartres", "Châteauroux", "Dreux", "Joué-lès-Tours", "Olivet", "Orléans", "Tours", "Vierzon"]
population = [45871, 64668, 38426, 43442, 30664, 38250, 22168, 116238, 136463, 25725]

def plus_grand_nb_habitant(liste_ville, population):
    """recherche la ville avec le nombre d'habitant le plus important

    Args:
        liste_ville (str): une liste de nom de ville
        population (_type_): une liste de nombre d'habitant correspondant au nombre d'habitant des villes du dessus

    Returns:
        str: le nom de la ville la plus peuplée
    """    
    population_max = 0
    indice_pop = None
    for i in range(len(population)):
        if population[i] > population_max:
            population_max = population[i]
            indice_pop = i
    return liste_ville[indice_pop]

def test_plus_grand_nb_habitant():
    assert plus_grand_nb_habitant(liste_villes, population) == "Tours"

#Exercice 3


def transfo_chaine_int(chaine):
    """retourne le nombre qui correspond a l'entrée en str en int

    Args:
        chaine (str): un nombre ecrit dans une chaine de caractere

    Returns:
        int: un nombr entier
    """
    entier = 0
    for chiffre in chaine:
        if chiffre == "0":
            entier = entier * 10 + 0
        elif chiffre == "1":
            entier = entier * 10 + 1
        elif chiffre == "2":
            entier = entier * 10 + 2
        elif chiffre == "3":
            entier = entier * 10 + 3
        elif chiffre == "4":
            entier = entier * 10 + 4
        elif chiffre == "5":
            entier = entier * 10 + 5
        elif chiffre == "6":
            entier = entier * 10 + 6
        elif chiffre == "7":
            entier = entier * 10 + 7
        elif chiffre == "8":
            entier = entier * 10 + 8
        elif chiffre == "9":
            entier = entier * 10 + 9
    return entier



def test_transfo_chaine_int():
    assert transfo_chaine_int("123") == 123
    assert transfo_chaine_int("0") == 0
    assert transfo_chaine_int("1") == 1
    assert transfo_chaine_int("9") == 9

liste_mot = ["salut","hello","hallo","ciao","hola"]
seconde_liste = ["matthias", 'm']


#Exercice 4

def recherche_mot(liste_mot, lettre):
    """recherche les mots commençant par la lettre donnée en argument

    Args:
        liste_mot (str): une liste de mot en minuscule
        lettre (str): une lettre en minuscule

    Returns:
        list: une liste de mot commençant par la lettre donnée en argument
    """    
    res_liste = []
    for mot in liste_mot:
        if mot[0] == lettre:
            res_liste.append(mot)
    return res_liste



def test_recherche_mot():
    assert recherche_mot(liste_mot, "h") == ["hello", "hallo", "hola"]
    assert recherche_mot(seconde_liste, "m") == ["matthias", 'm']
    assert recherche_mot(liste_mot, "a") == []


#Exercice 5

def decoupage_mot(phrase):
    """permet de decouper les mots d'une phrase en liste

    Args:
        phrase (str): une phrase

    Returns:
        list: liste des mots présents dans la phrase
    """    
    res_liste = []
    mot_en_cour = ""
    for lettre in phrase:
        if lettre.isalpha():
            mot_en_cour+=lettre
        else:
            if mot_en_cour != "":
                res_liste.append(mot_en_cour)
            mot_en_cour = ""
    return res_liste


def test_decoupage_mot():
    assert decoupage_mot("Cela fait déjà 28 jours! 28 jours à l’IUT’O! Cool!!") == ['Cela', 'fait', 'déjà', 'jours', 'jours', 'à', 'l', 'IUT', 'O', 'Cool']
    assert decoupage_mot('(3*2)+1') == []
    assert decoupage_mot('') == []


#Exercice 6


def recherche_mot_dans_phrase(phrase, lettre):
    """recherche les mot commencant par une certaine lettre dans une phrase

    Args:
        phrase (str): une phrase
        lettre (str): Une lettre

    Returns:
        liste: retourne la liste des mots commencant par la lettre donnée en parametre
    """
    return recherche_mot(decoupage_mot(phrase), lettre)

def test_recherche_mot_dans_phrase():
    assert recherche_mot_dans_phrase("Cela fait déjà 28 jours! 28 jours à l’IUT’O! Cool!!", "C") == ['Cela', 'Cool']

#Exercice 7

def crible_eratostene(n):
    #liste_res = [False, False]
    #if n == 0:
    #    liste_res = []
    #for i in range(1, n):
    #    liste_res.append(True)
    #return liste_res
    if n == 0:
        liste_res = []
    else:
        liste_res = [False, False] + [True] * (n - 1)
    return liste_res
    

def test_crible_eratostene():
    assert crible_eratostene(5) == [False, False, True, True, True, True]
    assert crible_eratostene(0) == []

#pour savoir si un nombre peut etre divisé par un autre on fait nombre % diviseur == 0
#si il est egale a 0 alors cela veut dire que c'est un multiple de ce nombre

def desactive_multiple(liste_boolean, entier):
    """met false a tout les multiple de l'entier

    Args:
        liste_boolean (list): une liste de boolean
        entier (int): un entier

    Returns:
        liste_boolean: une liste de boolean
    """
    for i in range(2*entier, len(liste_boolean), entier):#commence a 2 fois l'entier , jusqu'a la fin de la liste et d'un pas de l'entier
        liste_boolean[i] = False
    return liste_boolean


def test_est_un_multiple():
    assert desactive_multiple([False, False, True, True, True, True, True], 2) == [False, False, True, True, False, True, False]
    assert desactive_multiple([False, False, True, True, True, True, True], 3) == [False, False, True, True, True, True, False]


def implemente_crible(entier):
    """donne les nombre premiers jusqu'a l'entier en parametre

    Args:
        entier (int): un entier

    Returns:
        liste_res: une liste de nombre premier
    """    
    liste_res = []
    liste_boolean = crible_eratostene(entier)
    for i in range(2, len(liste_boolean)):
        desactive_multiple(liste_boolean, i)
    for i in range(len(liste_boolean)):
        if liste_boolean[i]:
            liste_res.append(i)
    return liste_res
    

def test_implement_crible():
    assert implemente_crible(6) == [2, 3, 5]
    assert implemente_crible(10) == [2, 3, 5, 7]
    assert implemente_crible(0) == []
    assert implemente_crible(20) == [2, 3, 5, 7, 11, 13, 17, 19]

print(implemente_crible(10))