import super_heros


avengers = {'Spider-man': (5, 5, 'araignée sympa du quartier'), 'Hulk': (7 , 4 , " Grand homme vert " ), 'M Melin ': (2 , 6, ' expert en archi ')}
dic_vide = {}
le_mien = {'Matthias': (10, 10, 'jadore python')}

def test_intellligence_moyenne():
    assert super_heros.intellligence_moyenne(avengers) == 5
    assert super_heros.intellligence_moyenne(dic_vide) == None
    assert super_heros.intellligence_moyenne(le_mien) == 10

def test_kikeleplusfort():
    assert super_heros.kikeleplusfort(avengers) == 'Hulk'
    assert super_heros.kikeleplusfort(dic_vide) == None
    assert super_heros.kikeleplusfort(le_mien) == 'Matthias'

def test_combienDeCretins():
    assert super_heros.combienDeCretins(avengers) == 1
    assert super_heros.combienDeCretins(dic_vide) == 0
    assert super_heros.combienDeCretins(le_mien) == 0