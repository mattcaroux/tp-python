def intellligence_moyenne(dictionnaire_super_heros):
    """permet de calculer la moyenne des inteligence des personnes du dictionnaire

    Args:
        dictionnaire_super_heros (dict): un dictionnaire de personne

    Returns:
        float: la moyenne des inteligence des personne du dictionnaire
    """
    somme = 0
    cpt_hero = 0
    for (heros, tuple) in dictionnaire_super_heros.items():
        somme += tuple[1]
        cpt_hero += 1
    if cpt_hero == 0:
        return None
    return somme / cpt_hero

def kikeleplusfort(dictionnaire_super_heros):
    """permet de savoir qui est le plus fort du dictionnaire

    Args:
        dictionnaire_super_heros (dict): un dictionnaire de personne

    Returns:
        str: la personne la plus forte
    """
    force_max = 0
    le_plus_fort = None
    for (heros, tuple) in dictionnaire_super_heros.items():
        if tuple[0] > force_max:
            le_plus_fort = heros
            force_max = tuple[0]
    return le_plus_fort

def combienDeCretins(dictionnaire_super_heros):
    """permet de savoir combien de personne sont en dessous de l'inteligence moyenne

    Args:
        dictionnaire_super_heros (dict): un dictionnaire de personne

    Returns:
        int: le nombre de personne qui ont une inteligence en dessous de la moyenne
    """
    cpt_cretins = 0
    #On utilise la fonction précédente pour avoir la moyenne du dictionnaire
    moyenne = intellligence_moyenne(dictionnaire_super_heros)
    for tuple in dictionnaire_super_heros.values():
        if tuple[1] < moyenne:
            cpt_cretins += 1
    return cpt_cretins
