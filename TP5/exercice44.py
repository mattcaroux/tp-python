scores = [352100, 325410, 312785, 220199, 127853]
joueurs = ['Batman', 'Robin', 'Batman', 'Joker', 'Batman']

def podium_joueur(liste_joueurs, liste_scores, joueur):
    """permet de connaiotre le podium du joueur passé en 
    parametre

    Args:
        liste_joueurs (list): une liste de joueurs
        liste_scores (list): une liste de scores
        joueur (str): un joueur

    Returns:
        int: Retourne le podium du joueur passé en paramètre, si le 
        joueur n'est pas dans la liste renvoie None
    """    
    meilleur_score = 0
    indice_podium = None
    if joueur not in liste_joueurs:
        return None
    for i in range(len(liste_scores)):
        if liste_scores[i] > meilleur_score and liste_joueurs[i] == joueur:
            meilleur_score = liste_scores[i]
            indice_podium = i
    return indice_podium + 1


def test_podium_joueur():
    assert podium_joueur(joueurs, scores, "Batman") == 1
    assert podium_joueur(joueurs, scores, "Robin") == 2
    assert podium_joueur(joueurs, scores, "Joker") == 4
    assert podium_joueur(joueurs, scores, "Matthias") == None