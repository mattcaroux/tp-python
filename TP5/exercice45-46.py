scores = [352100, 325410, 312785, 220199, 127853]
joueurs = ['Batman', 'Robin', 'Batman', 'Joker', 'Batman']

def indice_podium(score, liste_scores):
    """permet de connaitre l'indice ou sera placé le joueurs en fonction de
    son score

    Args:
        score (int): le score du joueur
        liste_scores (list): une liste de scores

        Returns:
        int: l'indice ou devra etre placé le joueurs
    """    
    for i in range(len(liste_scores)):
        if score > liste_scores[i]:
            return i
    return len(liste_scores)

print(indice_podium(314570, [352100, 325410, 312785, 220199, 127853]))

def test_indice_podium():
    assert indice_podium(314570, [352100, 325410, 312785, 220199, 127853]) == 2
    assert indice_podium(12, [352100, 325410, 312785, 220199, 127853]) == 5
    assert indice_podium(352101, [352100, 325410, 312785, 220199, 127853]) == 0
    assert indice_podium(352100, [352100, 325410, 312785, 220199, 127853]) == 1 #ce met en deuxieme


def place_sur_podium(score_joueur, joueur, liste_scores, liste_joueurs):
    print(liste_joueurs)
    indice_du_score = indice_podium(score_joueur,liste_scores)
    liste_joueurs.insert(indice_du_score, joueur)
    liste_scores.insert(indice_du_score, score_joueur)
    print(liste_joueurs)


print(place_sur_podium(314570, 'Matthias', scores, joueurs))
print(place_sur_podium(0, 'Matthias', scores, joueurs))
print(place_sur_podium(99999999, 'Matthias', scores, joueurs))