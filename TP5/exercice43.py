scores = [352100, 325410, 312785, 220199, 127853]
joueurs = ['Batman', 'Robin', 'Batman', 'Joker', 'Batman']

def occurence_joueur(liste_joueurs, joueur):
    """le nombre de fois que le joueurs est dans la liste des
    meilleurs joueurs

    Args:
        liste_joueurs (list): la liste des meilleurs joueurs
        joueur (str): un joueur

    Returns:
        int: le nombre de fois ou le joueurs à été trouvé dans
        la liste des meilleurs joueurs
    """    
    cpt = 0
    for j in liste_joueurs:
        if j == joueur:
            cpt += 1
    return cpt

def test_occurence_joueur():
    assert occurence_joueur(joueurs, 'Batman') == 3
    assert occurence_joueur(joueurs, 'Robin') == 1
    assert occurence_joueur(joueurs, '') == 0