# --------------------------------------
# Exemple de villes avec leur population
# --------------------------------------
liste_villes = ["Blois", "Bourges", "Chartres", "Châteauroux", "Dreux",
                "Joué-lès-Tours", "Olivet", "Orléans", "Tours", "Vierzon"]
population = [45871, 64668,  38426, 43442, 30664, 38250, 22168, 116238,
              136463,  25725]


def recherche_pop_ville(l_villes, liste_population, ville_recherche):
    """retrouve le nombre d'habitant d'une ville donné en parametre a partir de d'une liste de ville et une liste de nombre d'habitant

    Args:
        l_villes (str): une liste de ville
        liste_population (int): une liste de nombre d'habitant dans la ville correspondant a la ville du meme indice que l'autre liste
        ville_recherche (str): Une ville ou nous voulons connaitre son nombre d'habitant

    Returns:
        int: le nombre d'habitant de la ville passé en paramètre
    """    
    if len(liste_population) != len(liste_villes):
        return None
    position_ville = None
    for i in range(len(l_villes)):
        if l_villes[i] == ville_recherche:
            position_ville = i
    if position_ville != None:
        return liste_population[position_ville]
    else:
        return None

print(recherche_pop_ville(liste_villes, population, "Orléans"))

def test_recherche_pop_ville():
    assert recherche_pop_ville(liste_villes, population, "Orléans") == 116238
    assert recherche_pop_ville(liste_villes, population, "Blois") == 45871
    assert recherche_pop_ville(liste_villes, population, "Bourges") == 64668
    assert recherche_pop_ville(liste_villes, population, "Paris") == None
    assert recherche_pop_ville(liste_villes, population, "") == None
    assert recherche_pop_ville([], population, "Paris") == None
    assert recherche_pop_ville(liste_villes, [], "Luxembourg") == None
    assert recherche_pop_ville(["Paris", "Orléans"], [125469, 125469, 125469, 125469], "Orléans") == None











