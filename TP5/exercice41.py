scores = [352100, 325410, 312785, 220199, 127853]
joueurs = ['Batman', 'Robin', 'Batman', 'Joker', 'Batman']


def meilleur_score(liste_scores, liste_joueurs, joueur):
    """retourne le meilleur score du joueurs en parametre

    Args:
        liste_scores (list): une liste de score
        liste_joueurs (list): une liste de joueurs
        joueur (_type_): un joueur

    Returns:
        _type_: _description_
    """    
    meilleur_score = 0
    if joueur not in liste_joueurs:
        return None
    for i in range(len(liste_scores)):
        if liste_scores[i] > meilleur_score and liste_joueurs[i] == joueur:
            meilleur_score = liste_scores[i]
    return meilleur_score

def test_meilleur_score():
    assert meilleur_score(scores, joueurs, "Batman") == 352100
    assert meilleur_score(scores, joueurs, "Robin") == 325410
    assert meilleur_score(scores, joueurs, "Patoche") == None