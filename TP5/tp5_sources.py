def mystere(liste, valeur):
    """Retourne l'indice de la liste a partir du moment 
    ou la valeur est apparue 3 fois

    Args:
        liste ([type]): [une liste]
        valeur ([type]): [une valeur]

    Returns:
        [int]: [l'indice de la liste a partir du moment ou on a 3 fois la valeur]
    """
    xxx = 0
    yyy = 0
    for elem in liste:
        if elem == valeur:
            yyy += 1
            if yyy > 3:
                return xxx
        xxx += 1
    return None


def recherche_val(liste, valeur):
    """Retourne l'indice de la liste a partir du moment 
    ou la valeur est apparue 3 fois

    Args:
        liste ([type]): [une liste]
        valeur ([type]): [une valeur]

    Returns:
        [int]: [l'indice de la liste a partir du moment ou on a 3 fois la valeur]
    """
    occurence_val = 0
    for i in range(len(liste)):
        if liste[i] == valeur:
            occurence_val += 1
            if occurence_val > 3:
                return i
    return None


def test_mystere_recherche_val():
    assert mystere([12, 5, 8, 48, 12, 418, 185, 17, 5, 87], 20) == None
    assert mystere([], 20) == None
    assert mystere([2, 5, 8, 48, 12, 2, 185, 17, 5, 2], 2) == None #egal a 3 et pas superieur
    assert mystere([2, 5, 8, 2, 12, 2, 185, 17, 5, 2], 2) == 9
    assert recherche_val([12, 5, 8, 48, 12, 418, 185, 17, 5, 87], 20) == None
    assert recherche_val([], 20) == None
    assert recherche_val([2, 5, 8, 48, 12, 2, 185, 17, 5, 2], 2) == None #egal a 3 et pas superieur
    assert recherche_val([2, 5, 8, 2, 12, 2, 185, 17, 5, 2], 2) == 9 



mystere([12, 5, 8, 48, 12, 418, 185, 17, 5, 87], 20)

# --------------------------------------
# Exemple de villes avec leur population
# --------------------------------------
liste_villes = ["Blois", "Bourges", "Chartres", "Châteauroux", "Dreux",
                "Joué-lès-Tours", "Olivet", "Orléans", "Tours", "Vierzon"]
population = [45871, 64668,  38426, 43442, 30664, 38250, 22168, 116238,
              136463,  25725]

# ---------------------------------------
# Exemple de scores
# ---------------------------------------
scores = [352100, 325410, 312785, 220199, 127853]
joueurs = ['Batman', 'Robin', 'Batman', 'Joker', 'Batman']
