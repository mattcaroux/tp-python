scores = [352100, 325410, 312785, 220199, 127853]
joueurs = ['Batman', 'Robin', 'Batman', 'Joker', 'Batman']

def liste_bien_ordre_decroissant(liste):
    """permet de savoir si une liste est bien triee en ordre decroissant

    Args:
        liste (list): une liste

    Returns:
        bool: True si la liste est bien triée, False sinon
    """    
    if len(liste) == 0:
        return True
    for i in range(1, len(liste)):
        if liste[i] > liste[i-1]:
            return False
    return True

def test_liste_bien_ordre_decroissant():
    assert liste_bien_ordre_decroissant([352100, 325410, 312785, 220199, 127853])
    assert liste_bien_ordre_decroissant([])
    assert liste_bien_ordre_decroissant([1, 2]) == False
    assert liste_bien_ordre_decroissant([9, 8, 7, 6, 1, 2]) == False
    assert liste_bien_ordre_decroissant([1, 2, 9, 8, 7, 6]) == False