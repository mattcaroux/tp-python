def liste_bien_triee(liste):
    """permet de savoir si une liste est bien triee

    Args:
        liste (list): une liste

    Returns:
        bool: True si la liste est bien triée, False sinon
    """    
    if len(liste) == 0:
        return True
    for i in range(1, len(liste)):
        if liste[i] < liste[i-1]:
            return False
    return True


def test_liste_bien_triee():
    assert liste_bien_triee([1, 2, 3, 4]) == True
    assert liste_bien_triee([]) == True
    assert liste_bien_triee([2, 1]) == False
    assert liste_bien_triee([1, 2, 3, 4, 5, 1]) == False
    assert liste_bien_triee([5, 4, 3, 1, 2]) == False