def recherche_chiffre(chaine):
    """retourne l'indice du premier chiffre dans une
    chaine

    Args:
        chaine (_type_): _description_

    Returns:
        int: _description_
    """    
    for i in range(len(chaine)):
        if chaine[i].isdigit():
            return i
    return None

def test_recherche_chiffre():
    assert recherche_chiffre("on est le 30/09/2021") == 10
    assert recherche_chiffre("") == None
    assert recherche_chiffre("jdfreuigfiruezlgfy") == None
    assert recherche_chiffre("0234123.**/*58401") == 0