def mail_tester(chaine):
    """savoir si un mail est correct ou pas 

    Args:
        chaine (str): un mail

    Returns:
        bool: True si le mail est correct, False sinon
    """    
    nb_arobase = 0
    indice_arobase = None
    if ' ' in chaine or "@" not in chaine or "." not in chaine or chaine[0] == "@" or chaine[-1] == ".":
        return False
    for i in range(len(chaine)):
        if chaine[i] == "@":
            nb_arobase += 1
            indice_arobase = i
            if nb_arobase > 1:
                return False
    for i in range(indice_arobase, len(chaine)):
        if "." not in chaine:
            return False
    return True

def test_mail_tester():
    assert mail_tester("mattcaroux@gmail.com")
    assert mail_tester("mattcaroux") == False
    assert mail_tester(".matt") == False
    assert mail_tester("@qqch") == False
    assert mail_tester("je suis pas une @dress.e") == False