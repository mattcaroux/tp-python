def sum_liste_sup_seuil(liste, seuil):
    """savoir si la somme des element d'une liste est superieur au seuil 
    passé en parametre

    Args:
        liste (liste): une liste de nombre
        seuil (int): un seuil

    Returns:
        bool: True si le seuil est dépassé
    """    
    somme = 0
    for nb in liste:
        somme += nb
    return somme > seuil


def test_sum_liste_sup_seuil():
    assert sum_liste_sup_seuil([1, 4, 1, 2, 3, 4], 6) == True
    assert sum_liste_sup_seuil([], 12) == False
    assert sum_liste_sup_seuil([], 0) == False #ne depasse pas le seuil, il est égal
    assert sum_liste_sup_seuil([1, 4, 1, 2, 3, 4], 6000) == False