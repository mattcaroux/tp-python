# --------------------------------------
# DONNEES
# --------------------------------------

# exemple de liste d'oiseaux observables
oiseaux = [("Merle", "Turtidé"), ("Moineau", "Passereau"), ("Mésange", "Passereau"),
           ("Pic vert", "Picidae"), ("Pie", "Corvidé"), ("Pinson", "Passereau"),
           ("Rouge-gorge", "Passereau"), ("Tourterelle", "Colombidé")] 

# exemples de listes de comptage ces listes ont la même longueur que oiseaux
comptage1 = [2, 5, 0, 1, 2, 0, 3, 5]
comptage2 = [2, 1, 3, 0, 0, 3, 5, 1]
comptage3 = [0, 0, 4, 3, 2, 1, 2, 4]

# exemples de listes d'observations. Notez que chaque liste correspond à la liste de comptage de
# même numéro
observations1 = [("Merle", 2), ("Moineau", 5), ("Pic vert", 1), ("Pie", 2),
                 ("Rouge-gorge", 3), ("Tourterelle", 5)]

observations2 = [("Merle", 2), ("Mésange", 1), ("Moineau", 3),
                 ("Pinson", 3), ("Tourterelle", 5), ("Rouge-gorge", 1)]

observations3 = [("Mésange", 4), ("Pic vert", 3), ("Pie", 2), ("Pinson", 1),
                 ("Rouge-gorge", 2), ("Tourterelle", 4)]


# --------------------------------------
# FONCTIONS
# --------------------------------------

def oiseau_le_plus_observe(liste_observations):
    """ recherche le nom de l'oiseau le plus observé de la liste
        (si il y en a plusieur on donne le 1er trouve)

    Args:
        liste_observations (list): une liste de tuples (nom_oiseau, nb_observes)

    Returns:
        str: l'oiseau le plus observé (None si la liste est vide)
    """
    if len(liste_observations) == 0:
        return None
    oiseau_max = ("", 0)
    for observation in liste_observations:
        if observation[1] > oiseau_max[1]:
            oiseau_max = observation
    return oiseau_max[0]


def oiseau_le_plus_observe_v2(liste_observations):
    """ recherche le nom de l'oiseau le plus observé de la liste
        (si il y en a plusieur on donne le 1er trouve)

    Args:
        liste_observations (list): une liste de tuples (nom_oiseau, nb_observes)

    Returns:
        str: l'oiseau le plus observé (None si la liste est vide)
    """
    if len(liste_observations) == 0:
        return None
    oiseau_max = ("", 0)
    for i in range(len(liste_observations)):
        if liste_observations[i][1] > oiseau_max[1]:
            oiseau_max = liste_observations[i]
    return oiseau_max[0]

def recherche_oiseau(nom, oiseau_famille):
    """ retrouve la famille d'un oiseau grace à son nom

    Args:
        nom (str): le nom de l'oiseau
        oiseau_famille (liste): liste de tuples (nom, famille)

    Returns:
        tuple: le tuple (nom, famille) de l'oiseau
    """    
    for i in range(len(oiseau_famille)):
        if oiseau_famille[i][0] == nom:
            return oiseau_famille[i][1]
    return None

def recherche_par_famille(famille, oiseau_famille):
    """ retrouve les oiseaux d'une famille

    Args:
        famille (str): le nom de la famille
        oiseau_famille (liste): liste de tuples (nom, famille)

    Returns:
        list: _description_
    """    
    liste_oiseau = []
    for i in range(len(oiseau_famille)):
        if oiseau_famille[i][1] == famille:
            liste_oiseau.append(oiseau_famille[i][0])
    return liste_oiseau


def est_liste_observations(liste_observation):
    for i in range(1, len(liste_observation)):
        if liste_observation[i][0] < liste_observation[i-1][0]:
            return False
        if liste_observation[i][1] < 1:
            return False
    return True 

def max_observations(liste_observations):
    max_oiseau_observe = 0
    for i in range(len(liste_observations)):
        if liste_observations[i][1] > max_oiseau_observe:
            max_oiseau_observe = liste_observations[i][1]
    return max_oiseau_observe


def moyenne_oiseaux_observes(liste_observations):
    somme = 0
    for i in range(len(liste_observations)):
        somme += liste_observations[i][1]
    if somme == 0 or len(liste_observations) == 0:
        return None
    return somme/len(liste_observations)

def total_famille(liste_oiseau, liste_observation, famille):
    res = 0
    for i in range(len(liste_observation)):
        if recherche_oiseau(liste_observation[i][0], liste_oiseau) == famille:
            res += liste_observation[i][1]
    return res


def construire_liste_observations(liste_oiseaux, liste_comptage):
    liste_res = []
    for i in range(len(liste_oiseaux)):
        if liste_comptage[i]:
            liste_res.append((liste_oiseaux[i][0], liste_comptage[i]))
    return liste_res

def creer_ligne_sup(liste_oiseau):
    liste_res = []
    for i in range(len(liste_oiseau)):
        print("Combien de " + liste_oiseau[i][0] + " as tu observé")
        nombre_entree = int(input())
        if nombre_entree != 0:
            liste_res.append((liste_oiseau[i][0], nombre_entree))
        else:
            print("Je ne l'ajoute pas a la liste alors")
    return liste_res

#print(creer_ligne_sup(oiseaux))

def affichage_observation(liste_oiseau, liste_observation):
    for i in range(len(liste_observation)):
        print(("Nom: " + liste_oiseau[i][0]).ljust(25) + ("Famille: " + liste_oiseau[i][1]).ljust(25) + ("Nb observés : " + str(liste_observation[i][1])))
    return liste_observation, liste_oiseau

#print(affichage_observation(oiseaux, observations1))

def creer_chaine(liste_observation, seuil):
    """permet de creer une chaine de caractere qui lorsque le caractere **
    est present signifie que le nombre d'animal est superieur a ce seuil

    Args:
        liste_observation (list): une liste d'observation
        seuil (int): un entier

    Returns:
        str: la chaine de carac qui represente les animaux selon si
        leurs nombre depasse ou non le seuil
    """
    chaine = ""
    for i in range(len(liste_observation)):
        if liste_observation[i][1] >= seuil:
            chaine += "**  "
        else:
            chaine += "    "
    return chaine

#print(creer_chaine(observations1, 4))



def id_oiseau(liste_observation):
    chaine = ""
    for i in range(len(liste_observation)):
        chaine += liste_observation[i][0][0] + liste_observation[i][0][1] + liste_observation[i][0][2] + " "
    return chaine



def affichage_terminal(liste_observation):
    max = max_observations(liste_observation)
    i = 0
    while i < max:
        print((creer_chaine(liste_observation, max-i)).ljust(5))
        i += 1
    print(id_oiseau(liste_observation).ljust(1))

affichage_terminal(observations1)

#--------------------------------------
# PROGRAMME PRINCIPAL
#--------------------------------------

# afficher_graphique_observation(construire_liste_observations(oiseaux, comptage3))
# observes = saisie_observations(oiseaux)
# afficher_graphique_observation(observes)
# afficher_observations(oiseaux, observes)
