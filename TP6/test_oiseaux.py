import oiseaux
# --------------------------------------
# FONCTIONS
# --------------------------------------

def test_recherche_oiseau():
    assert oiseaux.recherche_oiseau('Merle', oiseaux.oiseaux)== 'Turtidé'

def test_recherche_par_famille():
    assert oiseaux.recherche_par_famille('Turtidé', oiseaux.oiseaux)== ['Merle']

def test_oiseau_le_plus_observe():
    assert oiseaux.oiseau_le_plus_observe(oiseaux.observations1)=="Moineau"
    assert oiseaux.oiseau_le_plus_observe(oiseaux.observations2)=="Tourterelle"
    assert oiseaux.oiseau_le_plus_observe(oiseaux.observations3)=="Mésange"
    assert oiseaux.oiseau_le_plus_observe([])==None

def test_oiseau_le_plus_observe_v2():
    assert oiseaux.oiseau_le_plus_observe_v2(oiseaux.observations1)=="Moineau"
    assert oiseaux.oiseau_le_plus_observe_v2(oiseaux.observations2)=="Tourterelle"
    assert oiseaux.oiseau_le_plus_observe_v2(oiseaux.observations3)=="Mésange"
    assert oiseaux.oiseau_le_plus_observe_v2([])==None

def test_est_liste_observations():
    assert oiseaux.est_liste_observations([("a", 2), ("b", 5), ("c", 1), ("d", 2),
                 ("e", 3), ("z", 5)])== True

def test_max_observations():
    assert oiseaux.max_observations([("a", 2), ("b", 5), ("c", 1), ("d", 2),
                 ("e", 3), ("z", 5)])== 5

def test_moyenne_oiseaux_observes():
    assert oiseaux.moyenne_oiseaux_observes([("a", 2), ("b", 5), ("c", 1), ("d", 2),
                 ("e", 3), ("z", 5)])==18/6

def test_total_famille():
    assert oiseaux.total_famille(oiseaux.oiseaux, oiseaux.observations1, 'Turtidé')== 2


def test_construire_liste_observations():
    assert oiseaux.construire_liste_observations([("Merle", "Turtidé"), ("Moineau", "Passereau"), ("Mésange", "Passereau"),
           ("Pic vert", "Picidae"), ("Pie", "Corvidé"), ("Pinson", "Passereau"),
           ("Rouge-gorge", "Passereau"), ("Tourterelle", "Colombidé")], [2, 5, 0, 1, 2, 0, 3, 5])== [('Merle', 2), ('Moineau', 5), ('Pic vert', 1), ('Pie', 2), ('Rouge-gorge', 3), ('Tourterelle', 5)]

#def test_creer_ligne_sup():
#    assert oiseaux.creer_ligne_sup(...)==...

#def test_creer_ligne_noms_oiseaux():
#    assert oiseaux.creer_ligne_noms_oiseaux(...)==...

def test_id_oiseau():
    assert oiseaux.id_oiseau(oiseaux.observation1) == "Mer Moi Pic Pie Rou Tou"