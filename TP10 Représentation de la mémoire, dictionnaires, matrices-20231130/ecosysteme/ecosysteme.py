"""
Init Dev : TP10
Exercice 2 : Ecosystème
"""

def extinction_immediate(ecosysteme, animal):
    """
    renvoie True si animal s'éteint immédiatement dans l'écosystème faute
    de nourriture
    """
    if animal not in ecosysteme.keys():
        return None
    if ecosysteme[animal] is None:
        return False
    return ecosysteme[animal] not in ecosysteme.keys()


def en_voie_disparition(ecosysteme, animal):
    """
    renvoie True si animal s'éteint est voué à disparaitre à long terme
    """
    i = 0
    animal_test = animal
    while ecosysteme[animal_test] is not None and i < len(ecosysteme.items()):
        if extinction_immediate(ecosysteme, animal_test):
            return True
        if ecosysteme[animal_test] is None:
            return False
        else:
            animal_test = ecosysteme[animal_test]
        i += 1
    return False

ecosysteme_1 = { 'Loup': 'Mouton', 'Mouton': 'Herbe', 'Dragon': 'Lion', 'Lion': 'Lapin', 'Herbe': None, 'Lapin': 'Carotte', 'Requin': 'Surfer'}

print(en_voie_disparition(ecosysteme_1, 'Loup'))
    


def animaux_en_danger(ecosysteme):
    """ renvoie l'ensemble des animaux qui sont en danger d'extinction immédiate"""
    res = set()
    for mangeur in ecosysteme.keys():
        if extinction_immediate(ecosysteme, mangeur):
            res.add(mangeur)
    return res


def especes_en_voie_disparition(ecosysteme):
    """ renvoie l'ensemble des animaux qui sont en voués à disparaitre à long terme
    """
    res = set()
    for mangeur in ecosysteme.keys():
        if en_voie_disparition(ecosysteme, mangeur):
            res.add(mangeur)
    return res





