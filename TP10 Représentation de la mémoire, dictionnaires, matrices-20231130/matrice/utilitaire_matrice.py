import API_matrice2 as API
# import test_API_matrice as TESTé


def get_diagonale_principale(matrice): #en haut a gauche jusqu'en bas a droite
    res = []
    for i in range(API.get_nb_lignes(matrice)):
        res.append(API.get_val(matrice, i, i))
    return res


def get_diagonale_secondaire(matrice):
    res = []
    for i in range(0, API.get_nb_lignes(matrice)):
        res.append(API.get_val(matrice, i, API.get_nb_colonnes(matrice) - i - 1))
    return res

def transpose(matrice):
    la_transpose = []
    for i in range(API.get_nb_lignes(matrice)):
        for elem in API.get_colonne(matrice, i):
            la_transpose.append(elem)
    return la_transpose

def is_triangulaire_inf(matrice):
    for i in range(API.get_nb_lignes(matrice)):# i est le numero de la ligne courante
        for j in range(i+1, API.get_nb_colonnes(matrice)):# j est le numero de la colonne courante
            if API.get_val(matrice, i, j) != 0:
                return False
    return True

def bloc(mat, ligne, colonne, hauteur, largeur):
    if ligne < 0 or hauteur + ligne > API.get_nb_lignes(mat):# On commence par regarder si la sous matrice est plus grande que la matrice en parametre
        return None
    if colonne < 0 or largeur + colonne > API.get_nb_colonnes(mat):#ici aussi mais en colonne 
        return None
    res = API.matrice(hauteur, largeur, None)
    cpt_ligne = 0
    for i in range(ligne, ligne+hauteur):
        cpt_colonne = 0
        for j in range(colonne, colonne+largeur):
            API.set_val(res, cpt_ligne, cpt_colonne, API.get_val(mat, i, j))
            cpt_colonne +=1
        cpt_ligne +=1
    return res


def somme(matrice1, matrice2):
    if API.get_nb_colonnes(matrice1) != API.get_nb_colonnes(matrice2) or API.get_nb_lignes(matrice1) != API.get_nb_lignes(matrice2):
        return None
    res_matrice = API.matrice(API.get_nb_lignes(matrice1), API.get_nb_colonnes(matrice1), None)
    for indice_ligne in range(API.get_nb_lignes(matrice1)):
        for indice_colonne in range(API.get_nb_colonnes(matrice1)):
            val = API.get_val(matrice1, indice_ligne, indice_colonne)
            val2 = API.get_val(matrice2, indice_ligne, indice_colonne)
            somme = val + val2
            API.set_val(res_matrice, indice_ligne, indice_colonne, somme)
    return res_matrice

def produit(matrice1, matrice2):
    if API.get_nb_colonnes(matrice1) != API.get_nb_lignes(matrice2):
        return None
    res_matrice = API.matrice(API.get_nb_lignes(matrice1), API.get_nb_colonnes(matrice1), None)
    for i in range(API.get_nb_lignes(res_matrice)):
        for j in range(API.get_nb_colonne(res_matrice)):
            val = 0
            for k in range(API.get_nb_colonnes(matrice1)):#ou get nb ligne de matrice 2
                val += API.get_val(matrice1, i, k)*API.get_val(matrice2, k, j)
            API.set_val(res_matrice, i, j, val)
    return res_matrice



# ---------------TEST MATRICE-------------------------


def test_matrice():
    assert API.matrice(3, 4, 0) == ([[0, 0, 0, 0], [0, 0, 0, 0], [0, 0, 0, 0]])


# matrice_test = (3, 4, [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11])
# matrice_test2 = (2, 2, [0, 1, 2, 3])
# matrice_test3 = (3, 3, [0, 1, 2, 3, 4, 5, 6, 7, 8])


def matrice1():
    """ définition d'une matrice pour les tests """
    mat1 = API.matrice(3, 3, None)
    API.set_val(mat1, 0, 0, 10)
    API.set_val(mat1, 0, 1, 11)
    API.set_val(mat1, 0, 2, 12)
    API.set_val(mat1, 1, 0, 13)
    API.set_val(mat1, 1, 1, 14)
    API.set_val(mat1, 1, 2, 15)
    API.set_val(mat1, 2, 0, 16)
    API.set_val(mat1, 2, 1, 17)
    API.set_val(mat1, 2, 2, 18)
    return mat1


# 10, 11, 12
# 13, 14, 15
# 16, 17, 18


def test_get_diagonale_principale():
    assert get_diagonale_principale(matrice1()) == [10, 14, 18]


def matrice_triangulaire_inferieur():
    """ définition d'une matrice pour les tests """
    matrice_inf = API.matrice(3, 3, None)
    API.set_val(matrice_inf, 0, 0, 1)
    API.set_val(matrice_inf, 0, 1, 0)
    API.set_val(matrice_inf, 0, 2, 0)
    API.set_val(matrice_inf, 1, 0, 1)
    API.set_val(matrice_inf, 1, 1, 1)
    API.set_val(matrice_inf, 1, 2, 0)
    API.set_val(matrice_inf, 2, 0, 1)
    API.set_val(matrice_inf, 2, 1, 1)
    API.set_val(matrice_inf, 2, 2, 0)
    return matrice_inf

# 1, 0, 0
# 1, 1, 0
# 1, 1, 0


matrice_triangulaire_inferieur = [[1, 0, 0], [1, 1, 0], [1, 1, 0]]

def test_is_triangulaire_inf():
    assert is_triangulaire_inf(matrice_triangulaire_inferieur) == True
    assert is_triangulaire_inf(matrice1()) == False

def test_bloc():
    assert bloc(matrice1(), 1, 1, 2, 2) == ([[14, 15], [17, 18]])