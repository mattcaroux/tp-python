def sante(taille, poids):
    """Permet de détécter des problème de santé en fonction de l'imc

    Args:
        taille (float): la taille de la personne en metre
        poids (_type_): le poid de la personne en kg

    Returns:
        str: le problème eventuelle détecté
    """    
    imc = poids/(taille*taille)
    if imc < 16.5:
        res = 'famine'
    elif imc < 18.5:
        res = 'maigreur'
    elif imc < 25:
        res = 'en bonne santé'
    elif imc < 30:
        res = "en surpoids"
    else :
        res = 'obésité'
    return res

def test_sante():
    assert sante(1.8, 80) == 'en bonne santé' #indique que sante 1.8, 80 doit retourner en bonne santé
    assert sante(1.6, 67) == 'en surpoids' #indique que sante(1.6, 67) doit retourner en surpoids
    #assert sante(2.2, 40) == "normal" #erreur volontaire