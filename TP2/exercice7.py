def contravention_exces_vitesse(vitesse_vehicule, vitesse_limite, recidive_sup_cinquante_km):
    """permet de calculer les contravention du aux exces de vitesse sur la route

    Args:
        vitesse_vehicule (float): la vitesse du vehicule
        vitesse_limite (float): la vitesse de la route sur laquelle a été pris le vehicule
        recidive_sup_cinquante_km (int): le nombre de fois où la personne qui va recevoir la contravention à été puni pour un depassement de plus de 50 km/h
    """
    montant_amende = 0
    nb_pts_perdu = 0
    nb_annee_suspension = 0
    nb_annee_prison = 0
    vitesse_de_depassement = vitesse_vehicule - vitesse_limite
    if vitesse_vehicule <= 0 or vitesse_limite <0:
        return "Il y a un problème dans les valeurs"
    if vitesse_de_depassement == 20:
        montant_amende = 68
        nb_pts_perdu = 1
    elif vitesse_de_depassement > 20 and vitesse_de_depassement < 30:
        montant_amende = 135
        nb_pts_perdu = 2
    elif vitesse_de_depassement > 30 and vitesse_de_depassement < 40:
        montant_amende = 135
        nb_pts_perdu = 3
        nb_annee_suspension = 3
    elif vitesse_de_depassement > 40 and vitesse_de_depassement < 50:
        montant_amende = 135
        nb_pts_perdu = 4
        nb_annee_suspension = 3
    elif vitesse_de_depassement > 50 and recidive_sup_cinquante_km == 0:
        montant_amende = 1500
        nb_pts_perdu = 6
        nb_annee_suspension = 3
    elif vitesse_de_depassement > 50 and recidive_sup_cinquante_km > 0:
        montant_amende = 3750
        nb_pts_perdu = 6
        nb_annee_suspension = 3
        nb_annee_prison = 3
    return(montant_amende, nb_pts_perdu, nb_annee_suspension, nb_annee_prison)

print(contravention_exces_vitesse(0, 50, 0))

def test_contravention_exces_vitesse():
    assert contravention_exces_vitesse(50, 30, 0) == (68, 1, 0, 0) #si ma vitesse de depassement est de 20
    assert contravention_exces_vitesse(75, 50, 0) == (135, 2, 0, 0) #si ma vitesse de depassement est entre 20 et 30
    assert contravention_exces_vitesse(85, 50, 0) == (135, 3, 3, 0) #si ma vitesse de depassement est entre 30 et 40
    assert contravention_exces_vitesse(92, 50, 0) == (135, 4, 3, 0) #si ma vitesse de depassement est entre 40 et 50
    assert contravention_exces_vitesse(110, 50, 0) == (1500, 6, 3, 0) # si ma vitesse de depassement est au dessus de 50km/h
    assert contravention_exces_vitesse(110, 50, 1) == (3750, 6, 3, 3) #si ma vitesse de depassement est au dessus de 50km/h et que j'ai deja été au dessus de 50km/h de depassement
    assert contravention_exces_vitesse(0, 50, 1) == "Il y a un problème dans les valeurs" #si la vitesse du vehicule est pas bonne
    assert contravention_exces_vitesse(10000, -50, 1) == "Il y a un problème dans les valeurs" #si la vitesse du vehicule est pas bonne
