def est_qualifiee_au_jo(sexe, record_100m, champion, nb_courses_gagnee):
    """Permet de savoir si l'atlhete peut participer au JO 
    selon les criteres de qualification

    Args:
        sexe (str): h si c'est un homme f si c'est une femme
        record_100m (float): le temps en seconde
        champion (bool): si le participant est un champion dans la discipline ou non
        nb_courses_gagnee (int): le nombre de courses remporté par le participant

    Returns:
        _type_: _description_
    """    
    res = False
    if champion:
        res = True
    if sexe == 'h':
        if record_100m < 12 and nb_courses_gagnee >= 3:
            res = True
    if sexe == 'f':
        if record_100m < 15 and nb_courses_gagnee >= 3:
            res = True
    return res

print(est_qualifiee_au_jo('h', 16, False, 9))

def test_est_qualidiee_aux_jo():
    assert est_qualifiee_au_jo('h', 5, False, 9) == True
    assert est_qualifiee_au_jo('f', 5, False, 9) == True
    assert est_qualifiee_au_jo('h', 20, False, 9) == False #pas qualifié pour les JO
    assert est_qualifiee_au_jo('f', 52, True, 1) == True