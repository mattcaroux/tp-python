def recherche_min(a, b, c, d):
    """recherche le plus petit nombre entre les paramètre

    Args:
        a (int or float): nombre
        b (int or float): nombre
        c (int or float): nombre
        d (int or float): nombre

    Returns:
        int or float: le nombre le plus petit
    """    
    if a < b:
        res = a
    else:
        res = b
    if c < res:
        res = c
    if d < res:
        res = d
    return res

#print(recherche_min(1, 2, 3, 4))


def test_recherche_min():
    assert recherche_min(1, 2, 3, 4) == 1
    assert recherche_min(-10, -5, -20, -1) == -20
    assert recherche_min(0, 100, -50, 10) == -50
    #assert recherche_min(42) == 42 #erreur volontaire

def voyelle_plus_presente(mot):
    """savoir si il y a plus de voyelle que de consonne

    Args:
        mot (str): un mot ecrit en minuscule

    Returns:
        bool: Vrai si le nombre de voyelle > au nombre de consonne, Faux sinon
    """
    cpt = 0
    if mot == "":
        return "Il n'y a pas de mot"
    for lettre in mot:
        if lettre in 'aeiouy':
            cpt += 1
        else:
            cpt -= 1
    if cpt == 0:
        return 'Il y a autant de consonne que de voyelle'
    return cpt>0


print(voyelle_plus_presente("ab"))


def test_voyelle_plus_presente():
    assert voyelle_plus_presente("matthias") == False
    assert voyelle_plus_presente("aaaaaa") == True
    assert voyelle_plus_presente("bbbbbb") == False
    assert voyelle_plus_presente("") == "Il n'y a pas de mot"
    assert voyelle_plus_presente("aabbccee") == 'Il y a autant de consonne que de voyelle'