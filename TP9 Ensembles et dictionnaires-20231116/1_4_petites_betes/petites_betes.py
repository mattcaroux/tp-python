"""Init Dev : TP9"""


# ==========================
# Petites bêtes
# ==========================

def toutes_les_familles(pokedex):
    """détermine l'ensemble des familles représentées dans le pokedex

    Args:
        pokedex (list): liste de pokemon, chaque pokemon est modélisé par
        un couple de str (nom, famille)

    Returns:
        set: l'ensemble des familles représentées dans le pokedex
    """
    res = set()
    for (pokemon, famille) in pokedex:
        res.add(famille)
    return res

def nombre_pokemons(pokedex, famille):
    """calcule le nombre de pokemons d'une certaine famille dans un pokedex

    Args:
        pokedex (list): liste de pokemon, chaque pokemon est modélisé par
        un couple de str (nom, famille)
        famille (str): le nom de la famille concernée

    Returns:
        int: le nombre de pokemons d'une certaine famille dans un pokedex
    """
    res = 0
    for (pokemon, famille_courante) in pokedex:
        if famille_courante == famille:
            res += 1
    return res
    

def frequences_famille(pokedex):
    """Construit le dictionnaire de fréqeunces des familles d'un pokedex

    Args:
        pokedex (list): liste de pokemon, chaque pokemon est modélisé par
        un couple de str (nom, famille)

    Returns:
        dict: un dictionnaire dont les clés sont le nom de familles (str)
        et la valeur associée est le nombre de représentants de la famille (int)
    """
    dictionnaire_frequence = dict()
    for (pokemon, famille) in pokedex:
        if famille not in dictionnaire_frequence.keys():
            dictionnaire_frequence[famille] = 1
        else:
            dictionnaire_frequence[famille] += 1
    return dictionnaire_frequence

def dico_par_famille(pokedex):
    """Construit un dictionnaire dont les les clés sont le nom de familles (str)
    et la valeur associée est l'ensemble (set) des noms des pokemons de cette
    famille dans le pokedex

    Args:
        pokedex (list): liste de pokemon, chaque pokemon est modélisé par
        un couple de str (nom, famille)

    Returns:
        dict: un dictionnaire dont les clés sont le nom de familles (str) et la valeur associée est
        l'ensemble (set) des noms des pokemons de cette famille dans le pokedex
    """
    dico_famille = dict()
    for (pokemon, famille) in pokedex:
        if famille not in dico_famille.keys():
            dico_famille[famille] = set()
        dico_famille[famille].add(pokemon)
    return dico_famille
        

def famille_la_plus_representee(pokedex):
    """détermine le nom de la famille la plus représentée dans le pokedex

    Args:
        pokedex (list): liste de pokemon, chaque pokemon est modélisé par
        un couple de str (nom, famille)

    Returns:
        str: le nom de la famille la plus représentée dans le pokedex
    """
    # max_occurence_famille = 0
    # nom_famille_max = None
    # for i in range(len(pokedex)):
    #     cpt_famille = 0
    #     famille = pokedex[i][1]
    #     for j in range(len(pokedex)):
    #         if pokedex[j][1] == famille:
    #             cpt_famille += 1
    #     if cpt_famille > max_occurence_famille:
    #         max_occurence_famille = cpt_famille
    #         nom_famille_max = famille
    # return nom_famille_max

    max = 0
    famille_max = None
    dico = dict()
    for (pokemon, famille) in pokedex:
        if famille not in dico.keys():
            dico[famille] = 1
        else:
            dico[famille] += 1
        if dico[famille] > max:
            max = dico[famille]
            famille_max = famille
    return famille_max




# ==========================
# Petites bêtes (la suite)
# ==========================


def toutes_les_familles_v2(pokedex):
    """détermine l'ensemble des familles représentées dans le pokedex

    Args:
        pokedex (dict): un dictionnaire dont les clés sont les noms de pokemons et la
        valeur associée l'ensemble (set) de ses familles (str)

    Returns:
        set: l'ensemble des familles représentées dans le pokedex
    """
    tous_les_types = set()
    for type in pokedex.values():
        tous_les_types = tous_les_types | type
    return tous_les_types

def nombre_pokemons_v2(pokedex, famille):
    """calcule le nombre de pokemons d'une certaine famille dans un pokedex

    Args:
        pokedex (dict): un dictionnaire dont les clés sont les noms de pokemons et la
        valeur associée l'ensemble (set) de ses familles (str)
        famille (str): le nom de la famille concernée

    Returns:
        int: le nombre de pokemons d'une certaine famille dans un pokedex
    """
    cpt = 0
    for type in pokedex.values():
        if famille in type:
            cpt += 1
    return cpt

def frequences_famille_v2(pokedex):
    """Construit le dictionnaire de fréqeunces des familles d'un pokedex

    Args:
        pokedex (dict): un dictionnaire dont les clés sont les noms de pokemons et la
        valeur associée l'ensemble (set) de ses familles (str)

    Returns:
        dict: un dictionnaire dont les clés sont le nom de familles (str) et la valeur
        associée est le nombre de représentants de la famille (int)
    """
    res = dict()
    for type in pokedex.values():
        for un_type in type:
            if un_type not in res.keys():
                res[un_type] = 1
            else:
                res[un_type] += 1
    return res


def dico_par_famille_v2(pokedex):
    """Construit un dictionnaire dont les les clés sont le nom de familles (str)
    et la valeur associée est l'ensemble (set) des noms des pokemons de
    cette famille dans le pokedex

    Args:
        pokedex (dict): un dictionnaire dont les clés sont les noms de pokemons et la
        valeur associée l'ensemble (set) de ses familles (str)

    Returns:
        dict: un dictionnaire dont les clés sont le nom de familles (str) et la valeur associée est
        l'ensemble (set) des noms des pokemons de cette famille dans le pokedex
    """
    dico_res = dict()
    for (pokemon, familles) in pokedex.items():
        for famille in familles:
            if famille not in dico_res.keys():
                dico_res[famille] = {pokemon}
            else:
                dico_res[famille].add(pokemon)
    return dico_res


def famille_la_plus_representee_v2(pokedex):
    """détermine le nom de la famille la plus représentée dans le pokedex

    Args:
        pokedex (dict): un dictionnaire dont les clés sont les noms de pokemons et la
        valeur associée l'ensemble (set) de ses familles (str)

    Returns:
        str: le nom de la famille la plus représentée dans le pokedex
    """
    dico_res = dict()
    max = 0
    famille_max = None
    for familles in pokedex.values():
        for famille in familles:
            if famille not in dico_res.keys():
                dico_res[famille] = 1
            else:
                dico_res[famille] += 1
            if dico_res[famille] > max:
                famille_max = famille
    return famille_max