import exercice3 as API

def sous_matrice(mat, ligne, colonne, hauteur, largeur):
    if ligne < 0 or hauteur + ligne > API.get_nb_lignes(mat):# On commence par regarder si la sous matrice est plus grande que la matrice en parametre
        return None
    if colonne < 0 or largeur + colonne > API.get_nb_colonnes(mat):#ici aussi mais en colonne 
        return None
    res = API.matrice(hauteur, largeur, None)
    cpt_ligne = 0
    for i in range(ligne, ligne+hauteur):
        cpt_colonne = 0
        for j in range(colonne, colonne+largeur):
            API.set_val(res, cpt_ligne, cpt_colonne, API.get_val(mat, i, j))
            cpt_colonne +=1
        cpt_ligne +=1
    return res


def colle_sous_matrice(matrice, sous_matrice, ligne_haut, colonne_gauche):
    if (ligne_haut + API.get_nb_lignes(sous_matrice)) > API.get_nb_lignes(matrice):
        return None
    for i in range(API.get_nb_lignes(sous_matrice)):
        for j in range(API.get_nb_colonnes(sous_matrice)):
            nouvelle_valeur = API.get_val(sous_matrice, i, j)
            API.set_val(matrice, ligne_haut+i, colonne_gauche+j, nouvelle_valeur)
    return matrice



matrice_test_1 = (3, 4, [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11])
matrice_zero = (2, 2, [0, 0, 0, 0])
matrice_un = (3, 3, [1, 1, 1, 1, 1, 1, 1, 1, 1])
trop_grande_matrice = (5, 3, [1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1])



#On affiche la matrice de base
API.affiche(matrice_test_1)
#On colle la matrice avec que des 0
colle_sous_matrice(matrice_test_1, matrice_zero, 0, 0)
API.affiche(matrice_test_1)
#On colle la matrice avec que des 1
colle_sous_matrice(matrice_test_1, matrice_un, 0, 0)
API.affiche(matrice_test_1)
#On fais volontairement un problème donc la matrice ne s'affichera pas
colle_sous_matrice(matrice_test_1, trop_grande_matrice, 0, 0)
API.affiche(matrice_test_1)
