# 3.1 il faut que la sous matrice soit <= à la matrice la plus grande
def sous_matrice(mat, ligne, colonne, hauteur, largeur):
    if ligne < 0 or hauteur + ligne > get_nb_lignes(mat):# On commence par regarder si la sous matrice est plus grande que la matrice en parametre
        return None
    if colonne < 0 or largeur + colonne > get_nb_colonnes(mat):#ici aussi mais en colonne 
        return None
    res = matrice(hauteur, largeur, None)
    cpt_ligne = 0
    for i in range(ligne, ligne+hauteur):
        cpt_colonne = 0
        for j in range(colonne, colonne+largeur):
            set_val(res, cpt_ligne, cpt_colonne, get_val(mat, i, j))
            cpt_colonne +=1
        cpt_ligne +=1
    return res

    

    
""" Matrices : API n 1 """


def matrice(nb_lignes, nb_colonnes, valeur_par_defaut):
    """crée une nouvelle matrice en mettant la valeur par défaut dans chacune de ses cases.

    Args:
        nb_lignes (int): le nombre de lignes de la matrice
        nb_colonnes (int): le nombre de colonnes de la matrice
        valeur_par_defaut : La valeur que prendra chacun des éléments de la matrice

    Returns:
        une nouvelle matrice qui contient la valeur par défaut dans chacune de ses cases
    """
    return (nb_lignes, nb_colonnes, [valeur_par_defaut]*(nb_lignes*nb_colonnes))



def set_val(la_matrice, ligne, colonne, nouvelle_valeur):
    """permet de modifier la valeur de l'élément qui se trouve à la ligne et à la colonne
    spécifiées. Cet élément prend alors la valeur nouvelle_valeur

    Args:
        la_matrice : une matrice
        ligne (int) : le numéro d'une ligne (la numérotation commence à zéro)
        colonne (int) : le numéro d'une colonne (la numérotation commence à zéro)
        nouvelle_valeur : la nouvelle valeur que l'on veut mettre dans la case

    Returns:
        None
    """
    la_matrice[2][ligne * la_matrice[1] + colonne] = nouvelle_valeur
    return None




def get_nb_lignes(la_matrice):
    """permet de connaître le nombre de lignes d'une matrice

    Args:
        la_matrice : une matrice

    Returns:
        int : le nombre de lignes de la matrice
    """
    return la_matrice[0]


def get_nb_colonnes(la_matrice):
    """permet de connaître le nombre de colonnes d'une matrice

    Args:
        la_matrice : une matrice

    Returns:
        int : le nombre de colonnes de la matrice
    """
    return la_matrice[1]


def get_val(la_matrice, ligne, colonne):
    """permet de connaître la valeur de l'élément de la matrice dont on connaît
    le numéro de ligne et le numéro de colonne.

    Args:
        la_matrice : une matrice
        ligne (int) : le numéro d'une ligne (la numérotation commence à zéro)
        colonne (int) : le numéro d'une colonne (la numérotation commence à zéro)

    Returns:
        la valeur qui est dans la case située à la ligne et la colonne spécifiées
    """
    return la_matrice[2][ligne * la_matrice[1] + colonne]

# Fonctions pour l'affichage

def affiche_ligne_separatrice(la_matrice, taille_cellule=4):
    """fonction auxilliaire qui permet d'afficher (dans le terminal)
    une ligne séparatrice

    Args:
        la_matrice : une matrice
        taille_cellule (int, optional): la taille d'une cellule. Defaults to 4.
    """
    print()
    for _ in range(get_nb_colonnes(la_matrice) + 1):
        print('-'*taille_cellule+'+', end='')
    print()


def affiche(la_matrice, taille_cellule=4):
    """permet d'afficher une matrice dans le terminal

    Args:
        la_matrice : une matrice
        taille_cellule (int, optional): la taille d'une cellule. Defaults to 4.
    """
    nb_colonnes = get_nb_colonnes(la_matrice)
    nb_lignes = get_nb_lignes(la_matrice)
    print(' '*taille_cellule+'|', end='')
    for i in range(nb_colonnes):
        print(str(i).center(taille_cellule) + '|', end='')
    affiche_ligne_separatrice(la_matrice, taille_cellule)
    for i in range(nb_lignes):
        print(str(i).rjust(taille_cellule) + '|', end='')
        for j in range(nb_colonnes):
            print(str(get_val(la_matrice, i, j)).rjust(taille_cellule) + '|', end='')
        affiche_ligne_separatrice(la_matrice, taille_cellule)
    print()


# Ajouter ici les fonctions supplémentaires, sans oublier de compléter le fichier
# tests_API_matrice.py avec des fonctions de tests

def charge_matrice_str(nom_fichier):
    """permet créer une matrice de str à partir d'un fichier CSV.

    Args:
        nom_fichier (str): le nom d'un fichier CSV (séparateur  ',')

    Returns:
        une matrice de str
    """
    l = 0
    c = 0
    ma_matrice = []
    fichier = open(nom_fichier, 'r')
    for ligne in fichier:
        l += 1
        c = len(ligne.strip().split(","))- 1
        ma_matrice += ligne.split(",")[:-1]
    fichier.close()
    return (l, c, ma_matrice)



def sauve_matrice(la_matrice, nom_fichier):
    """permet sauvegarder une matrice dans un fichier CSV.
    Attention, avec cette fonction, on perd l'information sur le type des éléments

    Args:
        matrice : une matrice
        nom_fichier (str): le nom du fichier CSV que l'on veut créer (écraser)

    Returns:
        None
    """
    matrice = la_matrice[2]
    nb_colonne = 0
    fic = open(nom_fichier, 'w') #créer ou efface le fichier nom_fichier
    for element_matrice in range(len(matrice)):
        nb_colonne += 1
        fic.write(matrice[element_matrice] + ',')#j'ajoute l'element de la liste au fichier en metant une virgule
        if nb_colonne == la_matrice[1]:
            fic.write("\n")#ajoute un espace apres que j'ai vu le nombre de colonne de la matrice
            nb_colonne = 0
    fic.close()


def get_ligne(matrice, ligne):
    res = []
    colonne = get_nb_colonnes(matrice)
    for i in range (colonne):
        res.append(get_val(matrice, ligne, i))
    return res



def get_colonne(matrice, colonne):
    res = []
    c = get_nb_colonnes(matrice)
    for i in range(colonne, get_nb_lignes(matrice), c):
        res.append(get_val(matrice, i, colonne))
    return res

matrice_test = (3, 4, [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11])
matrice_test2 = (2, 2, [0, 1, 2, 3])



# la transpose de matrice test 3
# 0, 3, 6,
# 1, 4, 7,
# 2, 5, 8

# 0 1  2  3
# 4 5  6  7
# 8 9 10 11

matrice_triangulaire_inferieur = (3, 3, [1, 0, 0, 1, 0, 0, 1, 1, 0])

def get_diagonale_principale(matrice): #en haut a gauche jusqu'en bas a droite
    res = []
    cpt = 0
    for i in range(0, get_nb_lignes(matrice), get_nb_colonnes(matrice)):
        res.append(get_val(matrice, cpt, i))
        cpt += 1
    return res


def get_diagonale_secondaire(matrice):
    res = []
    for i in range(0, get_nb_lignes(matrice)):
        res.append(get_val(matrice, i, get_nb_colonnes(matrice) - i - 1))
    return res

def transpose(matrice):
    la_transpose = []
    for i in range(get_nb_lignes(matrice)):
        for elem in get_colonne(matrice, i):
            la_transpose.append(elem)
    return la_transpose

def is_triangulaire_inf(matrice):
    for i in range(get_nb_lignes(matrice)):# i est le numero de la ligne courante
        for j in range(i+1, get_nb_colonnes(matrice)):# j est le numero de la colonne courante
            if get_val(matrice, i, j) != 0:
                return False
    return True