ATM = {'Armand':'Beatrice','Beatrice':'Cesar',
     'Cesar':'Dalida', 'Dalida':'Cesar',
     'Etienne':'Beatrice','Firmin':'Henri',
     'Gaston':'Beatrice','Henri':'Firmin'}

def couple_reciproque(dictionnaire_amoureux_timide):
    """permet de connaitre tous les couple d'un dictionnaire d'amoureux

    Args:
        dictionnaire_amoureux_timide (dict): un idctionnaire ou chaque membre
        renseigne le nom de la personne qu'il aime.

    Returns:
        set: un ensemble des couples reciproque
    """    
    res = set()#un ensemble pour pouvoir consulter les éléments plus rapidement
    for (amoureux, aimee) in dictionnaire_amoureux_timide.items():
        if aimee in dictionnaire_amoureux_timide.keys():#permet de savoir si la personne qui est aimée est dans le dictionnaire sinon retourne une erreur
            if dictionnaire_amoureux_timide[aimee] == amoureux:
                if (aimee, amoureux) not in res:
                    res.add((amoureux, aimee))
    return res


def soupirant(dictionnaire_amoureux_timide, personne):
    res = set()
    for personne_amoureuse, personne_aimme in dictionnaire_amoureux_timide.items():
        if personne_aimme == personne:
            res.add(personne_amoureuse)
    return res

print(soupirant(ATM, 'Beatrice'))