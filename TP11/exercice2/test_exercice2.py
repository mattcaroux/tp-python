import exercice2 as amoureux


ATM = {'Armand':'Beatrice','Beatrice':'Cesar',
     'Cesar':'Dalida', 'Dalida':'Cesar',
     'Etienne':'Beatrice','Firmin':'Henri',
     'Gaston':'Beatrice','Henri':'Firmin'}

ATM_vide = {}

aucun_couple_reciproque = {'Armand':'Beatrice','Beatrice':'Cesar'}



def test_couple_reciproque():
    assert amoureux.couple_reciproque(ATM) == {('Cesar', 'Dalida'), ('Firmin', 'Henri')}
    assert amoureux.couple_reciproque(ATM_vide) == set()
    assert amoureux.couple_reciproque(aucun_couple_reciproque) == set()