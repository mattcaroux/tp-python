# pierre = 15, 12, 8, 3 = 38 donc il doit : somme - 38 = +7,6€
# beatrice = 8 donc elle doit 30.4 - 8 =                 -22.4€
# sacha =                                                -30,4€
# Marie = 20, 34 donc somme - 54 =                       +23,6€
# Anna = 52 donc                                         +21,6€

# somme = 152 / 5 == 30.4



week_end_mai = {'Pierre' : [12, 70, 10], 'Paul' : [100], 'Marie' : [15], 'Anna' : [0]}
week_end_juin = {'Pierre' : [15, 12, 8, 3], 'Beatrice' : [8], 'Sacha' : [], 'Marie' : [20, 34], 'Anna' : [52]}


def affiche_bilan_fincancier(week_end):
    """Permet d'afficher le bilan financier pour un week end

    Args:
        week_end (dictionnaire): un disctionnaire qui a pour clés les nom de participant a ce week end
        et pour valeur une liste de leur depenses
    """
    total = depense_week_end(week_end)
    for personne in week_end.keys():
        a_payer = depense_week_end_par_personne(week_end, personne)
        if a_payer > total:
            print(personne + " doit recevoir " + str(a_payer-total) + " euros")
        else:
            print(personne + " doit verser " + str(total-a_payer) + " euros")
        


def depense_week_end(week_end):
    """calcul les depenses total pour un week end

    Args:
        week_end (dictionnaire): un disctionnaire qui a pour clés les nom de participant a ce week end
        et pour valeur une liste de leur depenses

    Returns:
        float: la somme des dépenses du week end
    """
    total = 0
    for personne in week_end.keys():
        total += depense_week_end_par_personne(week_end, personne)
    total = total / len(week_end.keys())
    return total



def depense_week_end_par_personne(week_end, personne):
    """la somme des depenses pour une personne

    Args:
        week_end (dictionnaire): un disctionnaire qui a pour clés les nom de participant a ce week end
        et pour valeur une liste de leur depenses
        personne (str): une personne du dictionnaire

    Returns:
        float: la somme des depense de la personne passé en paramètre
    """
    return sum(week_end[personne])
    



print(affiche_bilan_fincancier(week_end_mai))