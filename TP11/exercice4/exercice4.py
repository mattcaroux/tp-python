import time as t

def syracuse(n):
    res = []
    while n != 1:
        if n % 2 == 0:
            n = n // 2
            res.append(n)
        elif n % 2 == 1:
            n = 3 * n + 1
            res.append(n)
    return res



def temp_de_vol(n):
    return len(syracuse(n))



def champion(n):
    champion = n
    max_vol = 0
    for i in range(1, n-1):
        if temp_de_vol(i) > max_vol:
            max_vol = temp_de_vol(i)
            champion = i
    return champion



def meilleur_champion(n):
    temps = 0
    while temps < 1:
        temps = t.time()
        n -= 1
        temps = t.time() - temps
        print(temps)
    return n

def temp_de_vol_avec_precalcul(n, temp_connus):
    ...



print(temp_de_vol(124))