"""
Permet de modéliser un le_plateau de jeu avec :
    - une matrice qui contient des nombres entiers
    - chaque nombre entier correspond à un item :
      MUR, COULOIR, PERSONNAGE, FANTOME
"""
import matrice as API

MUR = 1
COULOIR = 0
PERSONNAGE = 2
FANTOME = 3

NORD = 'z'
OUEST = 'q'
SUD = 's'
EST = 'd'


def init(nom_fichier="./labyrinthe1.txt"):
    """Construit le plateau de jeu de la façon suivante :
        - crée une matrice à partir d'un fichier texte qui contient des COULOIR et MUR
        - met le PERSONNAGE en haut à gauche cad à la position (0, 0)
        - place un FANTOME en bas à droite
    Args:
        nom_fichier (str, optional): chemin vers un fichier csv qui contient COULOIR et MUR.
        Defaults to "./labyrinthe1.txt".

    Returns:
        le plateau de jeu avec les MUR, COULOIR, PERSONNAGE et FANTOME
    """
    plateau = API.charge_matrice(nom_fichier)
    API.set_val(plateau, 0, 0, PERSONNAGE)#On place le joueur
    API.set_val(plateau, API.get_nb_lignes(plateau)-1, API.get_nb_colonnes(plateau)-1, FANTOME)#On place le fantome
    return plateau

def init_deux_fantome(nom_fichier="./labyrinthe1.txt"):
    """Construit le plateau de jeu de la façon suivante :
        - crée une matrice à partir d'un fichier texte qui contient des COULOIR et MUR
        - met le PERSONNAGE en haut à gauche cad à la position (0, 0)
        - place un FANTOME en bas à droite
    Args:
        nom_fichier (str, optional): chemin vers un fichier csv qui contient COULOIR et MUR.
        Defaults to "./labyrinthe1.txt".

    Returns:
        le plateau de jeu avec les MUR, COULOIR, PERSONNAGE et FANTOME
    """
    plateau = API.charge_matrice(nom_fichier)
    API.set_val(plateau, 0, 0, PERSONNAGE)#On place le joueur
    API.set_val(plateau, API.get_nb_lignes(plateau)-1, API.get_nb_colonnes(plateau)-1, FANTOME)#On place le fantome
    API.set_val(plateau, 4, 2, FANTOME)#On place le deuxieme fantome
    return plateau



def est_sur_le_plateau(le_plateau, position):
    """Indique si la position est bien sur le plateau

    Args:
        le_plateau (plateau): un plateau de jeu
        position (tuple): un tuple de deux entiers de la forme (no_ligne, no_colonne) 

    Returns:
        [boolean]: True si la position est bien sur le plateau
    """
    if position[0] >= API.get_nb_lignes(le_plateau):
        return False
    if position[1] >= API.get_nb_colonnes(le_plateau):
        return False
    if position[0] < 0 or position[1] < 0:#test si la position est inferieur a 0
        return False
    return True


def get(le_plateau, position):
    """renvoie la valeur de la case qui se trouve à la position donnée

    Args:
        le_plateau (plateau): un plateau de jeu
        position (tuple): un tuple d'entiers de la forme (no_ligne, no_colonne)

    Returns:
        int: la valeur de la case qui se trouve à la position donnée ou
             None si la position n'est pas sur le plateau
    """
    try:
        return API.get_val(le_plateau, position[0], position[1])
    except:
        return None


def est_un_mur(le_plateau, position):
    """détermine s'il y a un mur à la poistion donnée

    Args:
        le_plateau (plateau): un plateau de jeu
        position (tuple): un tuple d'entiers de la forme (no_ligne, no_colonne)

    Returns:
        bool: True si la case à la position donnée est un MUR, False sinon
    """
    if get(le_plateau, position) == MUR:
        return True
    return False


def contient_fantome(le_plateau, position):
    """Détermine s'il y a un fantôme à la position donnée

    Args:
        le_plateau (plateau): un plateau de jeu
        position (tuple): un tuple de deux entiers de la forme (no_ligne, no_colonne) 

    Returns:
        bool: True si la case à la position donnée est un FANTOME, False sinon
    """
    if get(le_plateau, position) == FANTOME:
        return True
    return False

def est_la_sortie(le_plateau, position):
    """Détermine si la position donnée est la sortie
       cad la case en bas à droite du labyrinthe

    Args:
        le_plateau (plateau): un plateau de jeu
        position (tuple): un tuple de deux entiers de la forme (no_ligne, no_colonne) 

    Returns:
        bool: True si la case à la position donnée est la sortie, False sinon
    """
    if position == (API.get_nb_lignes(le_plateau)-1, API.get_nb_colonnes(le_plateau)-1):
        return True
    return False


def deplace_personnage(le_plateau, personnage, direction):
    """déplace le PERSONNAGE sur le plateau si le déplacement est valide
       Le personnage ne peut pas sortir du plateau ni traverser les murs
       Si le déplacement n'est pas valide, le personnage reste sur place

    Args:
        le_plateau (plateau): un plateau de jeu
        personnage (tuple): la position du personnage sur le plateau
        direction (str): la direction de déplacement SUD, EST, NORD, OUEST

    Returns:
        [tuple]: la nouvelle position du personnage
    """
    nouvelle_position = personnage
    if direction == SUD:
        position_voulue = (personnage[0]+1, personnage[1])
        if not est_un_mur(le_plateau, position_voulue) and est_sur_le_plateau(le_plateau, position_voulue):
            nouvelle_position = position_voulue
            API.set_val(le_plateau, position_voulue[0], position_voulue[1], PERSONNAGE)#On met le personnage a la nouvelle position
            API.set_val(le_plateau, personnage[0], personnage[1], COULOIR)#On enleve le personnage de sa position de base
    elif direction == NORD:
        position_voulue = (personnage[0]-1, personnage[1])
        if not est_un_mur(le_plateau, position_voulue) and est_sur_le_plateau(le_plateau, position_voulue):
            nouvelle_position = position_voulue
            API.set_val(le_plateau, position_voulue[0], position_voulue[1], PERSONNAGE)#On met le personnage a la nouvelle position
            API.set_val(le_plateau, personnage[0], personnage[1], COULOIR)#On enleve le personnage de sa position de base
    elif direction == EST:
        position_voulue = (personnage[0], personnage[1]+1)
        if not est_un_mur(le_plateau, position_voulue) and est_sur_le_plateau(le_plateau, position_voulue):
            nouvelle_position = position_voulue
            API.set_val(le_plateau, position_voulue[0], position_voulue[1], PERSONNAGE)#On met le personnage a la nouvelle position
            API.set_val(le_plateau, personnage[0], personnage[1], COULOIR)#On enleve le personnage de sa position de base
    elif direction == OUEST:
        position_voulue = (personnage[0], personnage[1]-1)
        if not est_un_mur(le_plateau, position_voulue) and est_sur_le_plateau(le_plateau, position_voulue):
            nouvelle_position = position_voulue
            API.set_val(le_plateau, position_voulue[0], position_voulue[1], PERSONNAGE)#On met le personnage a la nouvelle position
            API.set_val(le_plateau, personnage[0], personnage[1], COULOIR)#On enleve le personnage de sa position de base
    return nouvelle_position
        


def voisins(le_plateau, position):
    """Renvoie l'ensemble des positions cases voisines accessibles de la position renseignées
       Une case accessible est une case qui est sur le plateau et qui n'est pas un mur
    Args:
        le_plateau (plateau): un plateau de jeu
        position (tuple): un tuple de deux entiers de la forme (no_ligne, no_colonne) 

    Returns:
        set: l'ensemble des positions des cases voisines accessibles
    """
    res = set()
    if est_sur_le_plateau(le_plateau, (position[0], position[1]+1)) and not est_un_mur(le_plateau, (position[0], position[1]+1)):
        res.add((position[0], position[1]+1))
    if est_sur_le_plateau(le_plateau, (position[0], position[1]-1)) and not est_un_mur(le_plateau, (position[0], position[1]-1)):
        res.add((position[0], position[1]-1))
    if est_sur_le_plateau(le_plateau, (position[0]-1, position[1])) and not est_un_mur(le_plateau, (position[0]-1, position[1])):
        res.add((position[0]-1, position[1]))
    if est_sur_le_plateau(le_plateau, (position[0]+1, position[1])) and not est_un_mur(le_plateau, (position[0]+1, position[1])):
        res.add((position[0]+1, position[1]))
    return res


def fabrique_le_calque(le_plateau, position_depart):
    """fabrique le calque d'un labyrinthe en utilisation le principe de l'inondation :
       
    Args:
        le_plateau (plateau): un plateau de jeu
        position_depart (tuple): un tuple de deux entiers de la forme (no_ligne, no_colonne) 

    Returns:
        matrice: une matrice qui a la taille du plateau dont la case qui se trouve à la
       position_de_depart est à 0 les autres cases contiennent la longueur du
       plus court chemin pour y arriver (les murs et les cases innaccessibles sont à None)
    """
    calque = API.new_matrice(API.get_nb_lignes(le_plateau), API.get_nb_colonnes(le_plateau), None)
    i = 0
    API.set_val(calque, position_depart[0], position_depart[1], i)#on met la position de depart a 0
    il_y_a_des_voisin = True
    dico = dict()
    dico[0] = voisins(le_plateau, position_depart)
    while il_y_a_des_voisin:#mettre autre chose car la longueur du dictionnaire ne va pas bouger
        il_y_a_des_voisin = False
        for voisin in dico[i]:
            if get(calque, (voisin[0], voisin[1])) is None:
                API.set_val(calque, voisin[0], voisin[1], i+1)
                if i+1 in dico.keys():
                    dico[i+1] |= voisins(le_plateau, voisin)
                else:
                    dico[i+1] = voisins(le_plateau, voisin)
                if dico[i+1] != set():#me permet de savoir si il y a encore des voisins dans le dictionnaire d'apès
                    il_y_a_des_voisin = True
        i += 1
    return calque





def fabrique_chemin(le_plateau, position_depart, position_arrivee):
    """Renvoie le plus court chemin entre position_depart position_arrivee

    Args:
        le_plateau (plateau): un plateau de jeu
        position_depart (tuple): un tuple de deux entiers de la forme (no_ligne, no_colonne) 
        position_arrivee (tuple): un tuple de deux entiers de la forme (no_ligne, no_colonne) 

    Returns:
        list: Une liste de positions entre position_arrivee et position_depart
        qui représente un plus court chemin entre les deux positions
    """
    try:
        res = []
        res.append(position_arrivee)
        calque = fabrique_le_calque(le_plateau, position_depart)
        position_actuel = position_arrivee  # me permet de stocker la position du joueur en partant de la fin
        while position_actuel != position_depart:
            for voisin in voisins(le_plateau, position_actuel):
                if get(calque, voisin) == get(calque, position_actuel) - 1:
                    res.append(voisin)
                    position_actuel = voisin
            print(res)
        res.pop(-1)
        print(res)
        return res
    except Exception as e:
        print(f"Le plateau donné ne correspond pas a nos exigence ;) : {e}")
        return None

    
print(fabrique_chemin(init('./labyrinthe1.txt'), (0, 0), (8, 8)))

def deplace_fantome(le_plateau, fantome, personnage):
    """déplace le FANTOME sur le plateau vers le personnage en prenant le chemin le plus court

    Args:
        le_plateau (plateau): un plateau de jeu
        fantome (tuple): la position du fantome sur le plateau
        personnage (tuple): la position du personnage sur le plateau

    Returns:
        [tuple]: la nouvelle position du FANTOME
    """
    if fantome == personnage:
        return fantome
    chemin = fabrique_chemin(le_plateau, fantome, personnage)
    if chemin is not None:
        new_fantome = chemin[-1]
        API.set_val(le_plateau, fantome[0], fantome[1], COULOIR)#On enleve le fantome de sa position de base
        API.set_val(le_plateau, new_fantome[0], new_fantome[1], FANTOME)#On met le fantome a la nouvelle position
        print(chemin)
        return new_fantome
    return fantome


def deplace_fantome_deux_cases(le_plateau, fantome, personnage):
    """comme deplace fantome mais avance de deux cases

    Args:
        le_plateau (plateau): un plateau de jeu
        fantome (tuple): la position du fantome sur le plateau
        personnage (tuple): la position du personnage sur le plateau

    Returns:
        [tuple]: la nouvelle position du FANTOME
    """
    if fantome == personnage:
        return fantome
    chemin = fabrique_chemin(le_plateau, fantome, personnage)
    if chemin is not None:
        new_fantome = chemin[-2]
        API.set_val(le_plateau, fantome[0], fantome[1], COULOIR)#On enleve le fantome de sa position de base
        API.set_val(le_plateau, new_fantome[0], new_fantome[1], FANTOME)#On met le fantome a la nouvelle position
        print(chemin)
        return new_fantome
    return fantome