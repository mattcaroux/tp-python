from time import sleep
# Codé par Papy Force X, jeune padawan de l'informatique

def longueur_ok(mot_de_passe):
    """permet de savoir si le mot de passe est assez long

    Args:
        mot_de_passe (str): un mot de passe

    Returns:
        bool: True si la longueur du mot de passe est assez long
    """
    return len(mot_de_passe) >= 8

def chiffre_ok(mot_de_passe):
    """permet de savoir si le mot de passe contient 3 chiffre minimum

    Args:
        mot_de_passe (str): un mot de passe

    Returns:
        bool: True si le mot de passe contient bien 3 chiffre minimum
    """
    cpt_lettre = 0
    for lettre in mot_de_passe:
        if lettre.isdigit():
            cpt_lettre += 1
            if cpt_lettre >= 3:
                return True
    return False

def deux_chiffre_suivi(mot_de_passe):
    """permet de savoir si deux chiffre se suive

    Args:
        mot_de_passe (str): un mot de passe

    Returns:
        bool: True si les chiffres ne se suivent pas 
    """
    for i in range(1, len(mot_de_passe)):
        if mot_de_passe[i-1].isdigit() and mot_de_passe[i].isdigit():
            return False
    return True

def chiffre_min_1_occurence(mot_de_passe):
    """permet de savoir si le nombre le plus petit se repete plusieur fois

    Args:
        mot_de_passe (str): un mot de passe

    Returns:
        bool: Return true si le chiffre le plus petit n'apparait qu'une fois
    """
    min = 10
    occurence = 0
    for lettre in mot_de_passe:
        if lettre.isdigit():
            if int(lettre) < min:
                min = int(lettre)
                occurence = 1
            elif int(lettre) == min:
                occurence += 1
    return occurence == 1


def sans_espace(mot_de_passe):
    """permet de savoir si un mot de passe passé en parametre ne contient pas d'espace

    Args:
        mot_de_passe (str): un mot de passe

    Returns:
        bool: renvoie True si le mot de passe ne contient pas d'éspace
    """
    return ' ' not in mot_de_passe

def charger_mot_de_passe(nom_fichier):
    """permet de charger un fichier de mot de passe

    Args:
        nom_fichier (str): le nom d'un fichier dans le repertoire courant

    Returns:
        dict: un dictionnaire des avec login : mot_de_passe
    """
    dictionnaire = {}
    fic = open(nom_fichier, 'r')
    for ligne in fic:
        l_champs = ligne.strip().split(":")
        dictionnaire[l_champs[0]] = l_champs[1]
    fic.close()
    return dictionnaire


def sauver_mot_de_passe(login, mot_de_passe):
    """permet de sauvegarder un mot de passe dans un fichier

    Args:
        mot_de_passe (str): un mot de passe
    """
    #On créer un dictionnaire grâce à la fonction charger mot de passe
    dictionnaire = charger_mot_de_passe("mdpUltraSecret.txt")
    fic = open("mdpUltraSecret.txt", "w")
    #On utilise un dictionnaire pour mettre une nouvelle valeur à la clé et si elle n'est pas dans la liste elle sera donc créer
    dictionnaire[login] = mot_de_passe
    for clef, valeur in dictionnaire.items():
        fic.write(clef + ":" + valeur + "\n")
    fic.close()



def dialogue_mot_de_passe():
    login = input("Entrez votre nom : ")
    mot_de_passe_correct = False
    while not mot_de_passe_correct :
        mot_de_passe = input("Entrez votre mot de passe : ")
        if not longueur_ok(mot_de_passe):
            print("Votre mot de passe doit comporter au moins 8 caractères")
        elif not chiffre_ok(mot_de_passe):
            print("Votre mot de passe doit comporter au trois un chiffre")
        elif not sans_espace(mot_de_passe):
            print("Votre mot de passe ne doit pas comporter d'espace")	
        elif not deux_chiffre_suivi(mot_de_passe):
            print ("Le mot de passe ne doit pas comporter deux chiffre qui se suivent")   
        elif not chiffre_min_1_occurence(mot_de_passe):
            print("Le plus petit chiffre ne doit pas apparaitre plus d'1 fois")
        else:
            mot_de_passe_correct = True        
    for i in range(1, 100):
        print(f"Sauvegarde du mot de passe {i}%", end="\r")

        sleep(0.02)
    sauver_mot_de_passe(login, mot_de_passe)
    print("Votre mot de passe est correct")
    return mot_de_passe


if __name__ == '__main__':
    dialogue_mot_de_passe()