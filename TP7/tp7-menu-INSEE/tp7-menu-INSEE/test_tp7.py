"""Fonctions de test du TP7
ATTENTION VOUS DEVEZ COMPLETER LES TESTS
pour appeler vos fonctions ecrivez: tp7_source.ma_fonction
"""

import tp7_source

liste_communes= [('18001', 'aaa', 1200), ('18002', 'bbb', 71200),('18003', 'ccc', 520),
                ('45001', 'ddd', 85200), ('45002', 'abcd', 6350),('45003', 'aaa sur Loire', 8534),('45004', 'ggg', 5201)]

def test_population_d_une_commune():
    assert tp7_source.population_d_une_commune(liste_communes, 'ccc') == 520

def test_liste_des_communes_commencant_par():
    assert tp7_source.liste_des_communes_commencant_par(liste_communes, "a") == ["aaa", "abcd", "aaa sur Loire"]

def test_commune_plus_peuplee_departement():
    assert tp7_source.commune_plus_peuplee_departement(liste_communes, 18) == "bbb"

def test_nombre_de_communes_tranche_pop():
    assert tp7_source.nombre_de_communes_tranche_pop(liste_communes, 1000, 2000) == 1

def test_ajouter_trier():
    assert tp7_source.ajouter_trier(('18004', 'aaa', 2), [('18004', 'aaa', 3), ('18004', 'aaa', 1)], 10) == [('18004', 'aaa', 3), ('18004', 'aaa', 2), ('18004', 'aaa', 1)]
    assert tp7_source.ajouter_trier(('18004', 'aaa', 1200), liste_communes, 6) == liste_communes
    
print(tp7_source.top_n_population([('18001', 'aaa', 1200), ('18002', 'bbb', 71200), ('18003', 'ccc', 520)], 2))

def test_top_n_population():
    print(tp7_source.top_n_population([('18001', 'aaa', 1200), ('18002', 'bbb', 71200), ('18003', 'ccc', 520)], 2) == [('18002', 'bbb', 71200), ('18001', 'aaa', 1200)])
    assert tp7_source.top_n_population([('18001', 'aaa', 1200), ('18002', 'bbb', 71200), ('18003', 'ccc', 520)], 2) == [('18002', 'bbb', 71200), ('18001', 'aaa', 1200)]

def test_population_par_departement():
    ...
