""" TP7 une application complète
    ATTENTION VOUS DEVEZ METTRE DES DOCSTRING A TOUTES VOS FONCTIONS
"""
def afficher_menu(titre, liste_options):
    print("+"+"-"*(len(titre)+2) +"+")
    print("| " + titre + " |")
    print("+"+"-"*(len(titre)+2) +"+")
    for i in range(len(liste_options)):
        print(str(i+1) + " -> " + liste_options[i])
    
options = ["Charger un fichier", "Rechercher la population d'une commune", "Afficher la population d'un département", "Quitter"]



def demander_nombre(message, borne_max):#1.2
    entree = input(message)
    entree = int(entree)
    if entree > borne_max or entree < 1:
        return None
    return int(entree)

def menu(titre, liste_options): #1.3
    afficher_menu(titre, liste_options)
    nombre_choisi = demander_nombre("Choisissez une option : ", len(liste_options))
    return nombre_choisi
    
#print(menu("COUCOU", ["Manger", "Dormir"])) #renvoie bien 1 si 1 est l'entree

def programme_principal():
    liste_options = ["Charger un fichier",
                     "Rechercher la population d'une commune",
                     "Afficher la population d'un département",
                     "Trouver une commune à partir du debut du mot",
                     "Quitter"]
    liste_communes = []
    while True:
        rep = menu("MENU DE MON APPLICATION", liste_options)
        if rep is None:
            print("Cette option n'existe pas")
        elif rep == 1:
            print("Vous avez choisi", liste_options[rep - 1])
            try:
                nom_fichier = input("Entrez le nom du fichier que vous voulez charger : ")
            except ValueError:
                print("ce nom de ficher est incorrect")
            chargement = charger_fichier_population(nom_fichier)
            print("Il y a " + str(len(chargement)) + " commune")
        elif rep == 2:
            print("Vous avez choisi", liste_options[rep - 1])
            print("Choisissez entre ses options : ")
            print("1 -> Population de 2007")
            print("2 -> Population de 2017")
            choix_2 = input("Choisissez une option : ")
            if choix_2 == "1":
                liste_population = charger_fichier_population("population2007.csv")
            elif choix_2 == "2":
                liste_population = charger_fichier_population("population2017.csv")
            print("Le nombre d'habitant de la commune choisi est : " + str(population_d_une_commune(liste_population, input("Choisissez une commune : "))))
        elif rep == 3:
            print("Vous avez choisi", liste_options[rep - 1])
        elif rep == 4:
            liste_communes = liste_des_communes_commencant_par(charger_fichier_population("population2017.csv"), input("Rentrer le début du nom de la commune : "))
            print(liste_communes)
        else:
            break
        input("Appuyer sur Entrée pour continuer")
    print("Merci au revoir!")




def charger_fichier_population(nom_fic):
    liste_pop = []
    fichier = open(nom_fic, "r")
    fichier.readline()
    for ligne in fichier:
        l_depart = ligne.strip().split(";")
        liste_pop.append((l_depart[0], l_depart[1], int(l_depart[4])))
    fichier.close()
    return liste_pop


def population_d_une_commune(liste_pop, nom_commune):
    for commune in liste_pop:
        if commune[1] == nom_commune:
            return commune[-1]
    return None


def liste_des_communes_commencant_par(liste_pop, debut_nom):
    liste_nom = []
    for commune in liste_pop:
        if debut_nom.lower() == commune[1].lower()[0:len(debut_nom)]:#copyright shanka
            liste_nom.append(commune[1])
            print(commune[1])
    return liste_nom



def commune_plus_peuplee_departement(liste_pop, num_dpt):
    max = 0
    commune_max = ""
    #numero_dept = input("Choisissez le numéro de département : ")
    for commune in liste_pop:
        if str(num_dpt) == str(commune[0][0:len(str(num_dpt))]):
            if commune[-1] > max:
                max = commune[-1]
                commune_max = commune[1]
    return commune_max



def nombre_de_communes_tranche_pop(liste_pop, pop_min, pop_max):
    compteur = 0
    for info in liste_pop:
        if info[len(info)-1] >= pop_min and info[len(info)-1] <= pop_max:
            compteur += 1
    return compteur


def place_top(commune, liste_pop):
    for i in range(len(liste_pop)):
        if commune[2] > liste_pop[i][2]:
            return i
    return -1

def ajouter_trier(commune, liste_pop, taille_max):
    if len(liste_pop) < taille_max:
        liste_pop.insert(place_top(commune, liste_pop), commune)
    return liste_pop


def top_n_population(liste_pop, nbre):
    liste_triee = []
    liste_res = []
    i = 0
    for commune in liste_pop:
        liste_triee = ajouter_trier(commune, liste_triee, len(liste_pop))
    while len(liste_res) != 3:
        liste_res.append(liste_triee[i])
        i += 1
    return liste_res

def population_par_departement(liste_pop):
    ...

def sauve_population_dpt(nom_fic, liste_pop_dep):
    ...

# appel au programme principal
#programme_principal()
