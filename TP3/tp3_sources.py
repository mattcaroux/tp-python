# exercice 1
def mystere_exo2(entree):
    """[renvoie true si la liste contient plus de nombres pairs
    que de nombres impairs, false sinon]

    Args:
        entree ([float]): [un nombre]

    Returns:
        [bool]: [true si la liste contient plus de nombres pairs]
    """
    cpt_nb_pairs = 0
    cpt_nb_impaire = 0
    # au début de chaque tour de boucle
    #  A COMPLETER
    for nombre in entree:
        if nombre % 2 == 0:
            cpt_nb_pairs += 1
        else:
            cpt_nb_impaire += 1
    return cpt_nb_pairs >= cpt_nb_impaire


def test_mystere_exo2():
    assert mystere_exo2([1, 2, 3, 4, 5, 6, 7, 8, 9]) == False
    assert mystere_exo2([1, 2, 3, 4, 5, 6, 7, 8, 9, 10]) == True
    assert mystere_exo2([2, 4, 6, 8, 8, 3]) == True
    assert mystere_exo2([1, 3, 5, 7, 9, 3]) == False


# exercice 2
def min_sup(liste_nombres, valeur):
    """trouve le plus petit nombre d'une liste supérieur à une certaine valeur

    Args:
        liste_nombres (list): la liste de nombres
        valeur (int ou float): la valeur limite du minimum recherché

    Returns:
        int ou float: le plus petit nombre de la liste supérieur à valeur
    """
    # au début de chaque tour de boucle res est le plus petit élément
    # déjà énuméré supérieur à valeur
    res = None
    for elem in liste_nombres:
        if elem > valeur:
            if res is None:
                res = elem
            elif elem < res:
                res = elem
    return res


def test_min_sup():
    assert min_sup([8, 12, 7, 3, 9, 2, 1, 4, 9], 5) == 7
    assert min_sup([-2, -5, 2, 9.8, -8.1, 7], 0) == 2
    assert min_sup([5, 7, 6, 5, 7, 3], 10) is None
    assert min_sup([], 5) is None


# exercice 3

def nb_mots(phrase):
    """Trouver le nombre de mot dans une phrase

    Args:
        phrase (str): une phrase

    Returns:
        int : le nombre de mot dans une phrase
    """
    mot_en_cour = False
    cpt = 0
    for lettre in phrase:
        if lettre == " " and mot_en_cour:#si la lettre est un espace et que l'on etait dans un mot_en_cour
            mot_en_cour = False #mettre False dans mot_en_cour
        elif lettre != " " and not mot_en_cour:#sinon si la lettre n'est pas un espace et qu'on et pas dans un mot_en_cour
            mot_en_cour = True #mtn on est dans un mot
            cpt += 1
    return cpt



#print(nb_mots(" ce  test ne  marche pas "))

def test_nb_mots():
    assert nb_mots("bonjour, il fait beau") == 4
    assert nb_mots("houla!     je    mets beaucoup   d'  espaces    ") == 6
    assert nb_mots(" ce  test ne  marche pas ") == 5
    assert nb_mots("") == 0  # celui ci non plus