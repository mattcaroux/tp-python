def somme_premier_entiers(n):
    """Permet de retourner la somme des n premiers entiers

    Args:
        n (int): un entier

    Returns:
        int: la somme des n premiers entiers
    """
    somme = 0
    for i in range(n + 1):#si on ne met pas le +1 on aura la somme des n-1 premiers entiers
        somme += i
    return somme

def test_somme_premier_entiers():
    assert somme_premier_entiers(4) == 10
    assert somme_premier_entiers(0) == 0
    assert somme_premier_entiers(1) == 1
    assert somme_premier_entiers(2) == 3

def suite_de_syracuse(val_init, n):
    """_summary_

    Args:
        val_init (_type_): _description_
        n (_type_): _description_

    Returns:
        _type_: _description_
    """    

    for i in range(n):
        if val_init % 2 == 0:
            val_init = val_init / 2
        else:
            val_init = val_init * 3 + 1
    return val_init


# Test d'un autre algo qui est mauvais car trop long et complexe
#    compteur = 0
#    res = [val_init]
#    valeur_actuel = val_init
#    while compteur != n:
#        if valeur_actuel % 2 == 0:
#            valeur_actuel = valeur_actuel / 2
#            res.append(valeur_actuel)
#        else:
#            valeur_actuel = valeur_actuel * 3 + 1
#            res.append(valeur_actuel)
#        compteur+=1
#    print(res)
#    return res[-1]




print(suite_de_syracuse(1, 0))


def test_suite_de_syracuse():
    assert suite_de_syracuse(6, 2) == 10
    assert suite_de_syracuse(6, 3) == 5
    assert suite_de_syracuse(6, 4) == 16
    assert suite_de_syracuse(6, 5) == 8
    assert suite_de_syracuse(0, 0) == 0
    assert suite_de_syracuse(0, 1) == 0
    assert suite_de_syracuse(1, 0) == 1