def sum_nb_pairs(liste_nb):
    """Renvoie la somme des nombres pairs d'une liste d'entier

    Args:
        liste_nb (liste): une liste d'entiers

    Returns:
        int: la somme des nombres pairs de la liste
    """    
    somme = 0
    for nombre in liste_nb:
        if nombre % 2 == 0:
            somme += nombre
    return somme

def test_sum_nb_pairs():
    assert sum_nb_pairs([12, 13, 6, 5, 7]) == 18
    assert sum_nb_pairs([12, 13, 6, 5, 7, 8]) == 26
    assert sum_nb_pairs([1, 3, 5, 7, 9]) == 0
    assert sum_nb_pairs([]) == 0

def derniere_voyelle(chaine):
    """Permet de retourner la derniere voyelle d'une chaine de caractères

    Args:
        chaine (str): une chaine de caractères

    Returns:
        str: la derniere voyelle de la chaine
    """    
    voyelle = None
    for lettre in chaine:
        if lettre in 'aeiouy' or lettre in 'AEIOUY':
            voyelle = lettre
    return voyelle

def test_derniere_voyelle():
    assert derniere_voyelle("buongiorno") == "o"
    assert derniere_voyelle("bonjour") == "u"
    assert derniere_voyelle("cpt") == None
    assert derniere_voyelle("") == None
    assert derniere_voyelle("ALLLO") == "O"
    assert derniere_voyelle(" ce  test  marche ") == "e"


def ratio_nb_negatif(liste_nombres):
    """Permet de retourner le ratio de nombres négatifs dans une liste

    Args:
        liste_nombres (liste): une liste de nombres

    Returns:
        float: le ratio de nombres négatifs dans la liste
    """    
    cpt = 0
    if len(liste_nombres) == 0:
        return None
    for nombre in liste_nombres:
        if nombre < 0:
            cpt += 1
    return cpt/len(liste_nombres)

def test_ration_nb_negatif():
    assert ratio_nb_negatif([4, -2, 8, 2, -2, -7]) == 0.5
    assert ratio_nb_negatif([4, 2, 8, 2, 2, 7]) == 0
    assert ratio_nb_negatif([-4, -2, -8, -2, -2, -7]) == 1
    assert ratio_nb_negatif([]) == None