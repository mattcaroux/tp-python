def moyenne_nb_negatif(liste_nombre):
    """Calcule la moyenne des nombres négatifs d'une liste

    Args:
        liste_nombre (float): une liste de nombres

    Returns:
        _type_: Renvoie la moyenne des nombres négatifs de la liste
    """    
    somme_nb_negatifs = 0
    cpt_nb_negatifs = 0
    for nombre in liste_nombre:
        if nombre < 0:
            cpt_nb_negatifs += 1
            somme_nb_negatifs += nombre
    if cpt_nb_negatifs == 0:
        res =  0
    else:
        res = somme_nb_negatifs / cpt_nb_negatifs
    return res



def test_moyenne_nb_negatifs():
    assert moyenne_nb_negatif([1, 2, 3, 4, 5, 6, 7, 8, 9]) == 0
    assert moyenne_nb_negatif([-1, -4, -7, 9, 17, -3, 0]) == -3.75
    assert moyenne_nb_negatif([]) == 0


def cpt_syllabe(mot):
    """Compte le nombre de syllabes dans un mot

    Args:
        mot (str): un mot en minuscule

    Returns:
        int: le nombre de syllabes dans un mot
    """    
    cpt_syllabe = 0
    etat_voyelle = False
    for lettre in mot:
        if lettre in 'aeiouy' and etat_voyelle == False:
                cpt_syllabe += 1
        etat_voyelle = lettre in 'aeiouy'
    return cpt_syllabe


def test_cpt_syllabe():
    assert cpt_syllabe('ecouteur') == 3
    assert cpt_syllabe('tableau') == 2
    assert cpt_syllabe('bonjour') == 2
    assert cpt_syllabe(' ') == 0
    assert cpt_syllabe('') == 0